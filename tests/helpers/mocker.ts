// https://github.com/kulshekhar/ts-jest/issues/472#issuecomment-421960393
// @ts-ignore
export function mocked<T>(val: T): T extends (...args: any[]) => any ? jest.MockInstance<T> : jest.Mocked<T> {
    return val as any
}