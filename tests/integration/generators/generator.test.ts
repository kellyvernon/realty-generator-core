'use strict';

import {
    Generator, ICreateOptions,
    IUser,
    Ledger, LedgerItem, LedgerItemType, randomNumberBetween,
    RentalCollection,
    RentalGenerator,
    RentalTypes,
    User,
    ValueCache
} from '../../../src';

describe('generator tests', () => {
    let u: IUser;
    let date: Date;
    let gen: Generator;

    afterEach(() => {
        u = null;
        date = null;
    });

    describe('and simulate', () => {
        beforeEach(() => {
            date = new Date();
            u = new User({
                maxInvestAmount: 500000,
                minInvestAmount: 10000,
                minMonthlyCashOnCashAmountHouse: 200,
                minMonthlyCashOnCashAmountApartment: 200,
                minEquityCapturePercent: 50,
                monthlyIncomeAmountGoal: 4000
            });
            u.ledger = new Ledger();
            u.ledger.pushLedgerItem(new LedgerItem({
                amount: 100000,
                type: LedgerItemType.Income,
                created: new Date(date.getTime()),
                note: 'saved for month'
            }));
            u.rentals = new RentalCollection();
        });

        test('should get some houses', async () => {
            const valueCache = new ValueCache(new Date(date.getFullYear(), date.getMonth(), 1), [], 2);

            const rentalCacheCollection = [{
                rentalType: RentalTypes.SingleFamily,
                generator: new RentalGenerator(valueCache)
            }];

            gen = new Generator(rentalCacheCollection);

            const options = {
                highestMinRefinanceInMonths: 2,
                lowestMinRefinanceInMonths: 1,
                highestRefinancePercent: 60,
                lowestRefinancePercent: 30,
                renewalInMonths: 2,
                minRentalOpportunities: 7,
                maxRentalOpportunities: 13,
                lowestPriceDown: 100000,
                highestPriceDown: 134000,
                lowestSellPercent: 105,
                highestSellPercent: 110,
                lowestMinSellInYears: 3,
                highestMinSellInYears: 4,
                lowestCashOnCashPercent: 15,
                highestCashOnCashPercent: 20,
                translate: (createOptions: ICreateOptions) => {
                    const investedAmount = randomNumberBetween(20000, 40000);
                    const investmentPercent = investedAmount / createOptions.purchasePrice * 100;

                    return {
                        ...createOptions,
                        investmentPercent,
                        cashOnCashAmountMonthly: u.minMonthlyCashOnCashAmountHouse
                    };
                }
            };
            const actual = await gen.simulate(u, [{
                options,
                rentalType: RentalTypes.SingleFamily
            }], 15);

            expect(actual.rentals.length).toBeGreaterThan(50);
            expect(actual.user.rentals.length).toBeGreaterThan(2);
        });
        test('should generate some apartments',async  () => {
            const valueCache = new ValueCache(new Date(date.getFullYear(), date.getMonth(), 1), [], 2);

            const rentalCacheCollection = [{
                rentalType: RentalTypes.PassiveApartment,
                generator: new RentalGenerator(valueCache)
            }];

            gen = new Generator(rentalCacheCollection);

            const options = {
                highestMinRefinanceInMonths: 60,
                lowestMinRefinanceInMonths: 36,
                highestRefinancePercent: 60,
                lowestRefinancePercent: 50,
                renewalInMonths: 3,
                minRentalOpportunities: 2,
                maxRentalOpportunities: 10,
                lowestPriceDown: 8000000,
                highestPriceDown: 20000000,
                lowestSellPercent: 120,
                highestSellPercent: 300,
                lowestMinSellInYears: 4,
                highestMinSellInYears: 5,
                lowestCashOnCashPercent: 6,
                highestCashOnCashPercent: 8,
                translate: (createOptions: ICreateOptions) => {
                    return {
                        ...createOptions,
                        cashOnCashPercent: randomNumberBetween(options.lowestCashOnCashPercent, options.highestCashOnCashPercent),
                        investmentAmounts: [50000, 100000, 150000, 200000, 250000, 500000].filter(e => {
                            const highestAmountWithLoanAndClosingCostsAndRepairs = options.highestPriceDown / 3.333;
                            return e <= highestAmountWithLoanAndClosingCostsAndRepairs;
                        })
                    }
                }
            };
            const actual = await gen.simulate(u, [{
                options,
                rentalType: RentalTypes.PassiveApartment
            }]);

            expect(actual.rentals.length).toBeGreaterThan(100);
            expect(actual.user.rentals.length).toBeGreaterThan(2);
        });
    });
});
