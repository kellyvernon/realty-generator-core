'use strict';

import {LedgerItem} from '../../src/ledger';

describe('ledger item tests', () => {
    test('should resolve', () => {
        const l = new LedgerItem();
        expect(l.amount).toBe(0);
        expect(l.note).toBe('');
        expect(l.referenceId).toBe(null);
        const date = new Date(Date.now());
        expect(l.created).toEqual(new Date(date.getUTCFullYear(), date.getUTCMonth(), 1));
    });
});
