'use strict';

jest.mock('../../src/ledger/ledger');
jest.mock('../../src/rentals/rental-collection');

import {RentalCollection} from '../../src/rentals';
import {User} from '../../src';
import {Ledger} from '../../src/ledger';
import {mocked} from '../helpers/mocker';

describe('user tests', () => {
    test('should have null ledger', () => {
        const u = new User();
        expect(u.ledger).toEqual(null);
    });

    test('should return monthlyIncomeAmountGoal', () => {
        const monthlyIncomeAmountGoal = 1;
        const u = new User({
            monthlyIncomeAmountGoal
        });

        expect(u.monthlyIncomeAmountGoal).toEqual(monthlyIncomeAmountGoal);
    });

    test('should return minMonthlyCashOnCashAmount', () => {
        const minMonthlyCashOnCashAmount = 1;
        const u = new User({
            minMonthlyCashOnCashAmountApartment: minMonthlyCashOnCashAmount
        });

        expect(u.minMonthlyCashOnCashAmountApartment).toEqual(minMonthlyCashOnCashAmount);
    });

    test('should return minMonthlyCashOnCashAmount', () => {
        const minEquityCapturePercent = 1;
        const u = new User({
            minEquityCapturePercent
        });

        expect(u.minEquityCapturePercent).toEqual(minEquityCapturePercent);
    });

    test('should return minInvestAmount', () => {
        const minInvestAmount = 1;
        const u = new User({
            minInvestAmount
        });

        expect(u.minInvestAmount).toEqual(minInvestAmount);
    });

    test('should return maxInvestAmount', () => {
        const maxInvestAmount = 1;
        const u = new User({
            maxInvestAmount
        });

        expect(u.maxInvestAmount).toEqual(maxInvestAmount);
    });

    test('should return defaults', () => {
        const defaultAmount = 0;
        const u = new User();

        expect(u.monthlyIncomeAmountGoal).toEqual(defaultAmount);
        expect(u.minMonthlyCashOnCashAmountApartment).toEqual(defaultAmount);
        expect(u.minEquityCapturePercent).toEqual(defaultAmount);
        expect(u.maxInvestAmount).toEqual(defaultAmount);
        expect(u.maxInvestAmount).toEqual(defaultAmount);
    });

    describe('and metMonthlyGoal', () => {
        test('and no rental collection', () => {
            const defaultAmount = 2000;
            const u = new User({monthlyIncomeAmountGoal: defaultAmount});

            expect(u.metMonthlyGoal(new Date(Date.now()))).toBeFalsy();
        });

        test('and no date', () => {
            const defaultAmount = 2000;
            const u = new User({monthlyIncomeAmountGoal: defaultAmount});

            expect(u.metMonthlyGoal(null)).toBeFalsy();
        });

        test('and rental collection is empty', () => {
            const defaultAmount = 2000;
            const u = new User({monthlyIncomeAmountGoal: defaultAmount});
            u.rentals = new RentalCollection();

            expect(u.metMonthlyGoal(new Date(Date.now()))).toBeFalsy();
        });

        test('and rental collection is under', () => {
            const defaultAmount = 2000;
            const u = new User({monthlyIncomeAmountGoal: defaultAmount});
            u.rentals = new RentalCollection();
            // @ts-ignore
            u.rentals.cashOnCashAmountMonthly.mockReturnValue(defaultAmount - 1);

            expect(u.metMonthlyGoal(new Date(Date.now()))).toBeFalsy();
        });

        test('and rental collection is equal', () => {
            const defaultAmount = 2000;
            const u = new User({monthlyIncomeAmountGoal: defaultAmount});
            u.rentals = new RentalCollection();
            // @ts-ignore
            u.rentals.cashOnCashAmountMonthly.mockReturnValue(defaultAmount);

            expect(u.metMonthlyGoal(new Date(Date.now()))).toBeTruthy();
        });
    });

    describe('and hasMoneyToInvest', () => {
        describe('and no ledger', () => {
            test('and cannot invest', () => {
                const u = new User();

                expect(u.hasMoneyToInvest()).toBeFalsy();
            });
        });

        describe('and ledger total is zero', () => {
            test('and cannot invest', () => {
                const ledger = mocked(new Ledger());
                ledger.total.mockReturnValue(0);

                const u = new User();
                u.ledger = ledger;
                expect(u.hasMoneyToInvest()).toBeFalsy();
            });
        });

        describe('and ledger total is greater than zero', () => {
            test('and can invest', () => {
                const ledger = mocked(new Ledger());
                ledger.total.mockReturnValue(90);

                const u = new User();
                u.rentals = new RentalCollection();
                // @ts-ignore
                u.rentals.reserveRequirementMonthly.mockReturnValue(0);

                u.ledger = ledger;
                expect(u.hasMoneyToInvest()).toBeTruthy();
            });
        });
    });
});
