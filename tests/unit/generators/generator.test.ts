'use strict';

import Mocked = jest.Mocked;

jest.mock('../../../src/user');
jest.mock('../../../src/ledger/ledger');
jest.mock('../../../src/rentals/rental-collection');
jest.mock('../../../src/rentals/passive-apartment');
jest.mock('../../../src/utils/data-collection');
jest.mock('../../../src/generators/rental-generator');

import {Generator, IRunEntryResult} from '../../../src/generators/generator';
import {RentalGenerator} from '../../../src/generators/rental-generator';
import {Ledger} from '../../../src/ledger/ledger';
import {LedgerItem} from '../../../src/ledger/ledger-item';
import {LedgerItemType} from '../../../src/ledger/ledger-item-type';
import {RentalCollection} from '../../../src/rentals/rental-collection';
import {PassiveApartment} from '../../../src/rentals/passive-apartment';
import {RentalTypes} from '../../../src/rentals/rental-types';
import {IUser, User} from '../../../src/user';
import {randomNumberBetween} from '../../../src/utils/data-number';
import {getRentalsBySort, getRentalsUserCanNotInvest} from '../../../src/utils/data-collection';
import {IRentalCashFlow} from '../../../src/rentals/i-rental-cash-flow';
import {IRentalSale} from '../../../src/rentals/i-rental-sale';

describe('generator tests', () => {
    let u: IUser;
    let l: Ledger;
    let r: RentalCollection;
    let rentalGeneratorPassive;
    let g: Generator;
    let date;

    afterEach(() => {
        u = null;
        l = null;
        r = null;
        rentalGeneratorPassive = null;
        g = null;
        date = null;

        jest.restoreAllMocks();
    });

    describe('and runEntry', () => {
        describe('and user monthlySavedAmount', () => {
            beforeEach(() => {
                // @ts-ignore
                getRentalsBySort.mockReturnValue([]);
                // @ts-ignore
                getRentalsUserCanNotInvest.mockReturnValue([]);
                l = new Ledger();

                r = new RentalCollection();
                // @ts-ignore
                r.currentCashFlowByRental.mockReturnValue([]);
                // @ts-ignore
                r.rentalsForRefinance.mockReturnValue([]);
                // @ts-ignore
                r.rentalsForSale.mockReturnValue([]);

                u = new User();
                u.ledger = l;
                u.rentals = r;

                rentalGeneratorPassive = new RentalGenerator(null);
                // @ts-ignore
                rentalGeneratorPassive.getRentals.mockReturnValue([]);

                g = new Generator([{
                    generator: rentalGeneratorPassive,
                    rentalType: RentalTypes.PassiveApartment
                }]);

                date = new Date(Date.now());
            });

            describe('is greater than 0', () => {
                test('should add ledger', () => {
                    const expectedMonthlySavedAmount = randomNumberBetween(1, 100);
                    Object.defineProperty(u, 'monthlySavedAmount', {get: () => expectedMonthlySavedAmount});

                    g.runEntry({today: date, user: u});

                    expect(l.pushLedgerItem).nthCalledWith(1, {
                        amount: u.monthlySavedAmount,
                        type: LedgerItemType.Income,
                        created: date,
                        note: 'saved for month'
                    });
                });
            });

            describe('is than 0', () => {
                test('should not add ledger', () => {
                    g.runEntry({today: date, user: u});

                    expect(l.pushLedgerItem).not.toBeCalled();
                });
            });
        });

        describe('and rentals.currentCashFlowByRental', () => {
            beforeEach(() => {
                // @ts-ignore
                getRentalsBySort.mockReturnValue([]);

                l = new Ledger();

                r = new RentalCollection();
                // @ts-ignore
                r.rentalsForRefinance.mockReturnValue([]);
                // @ts-ignore
                r.rentalsForSale.mockReturnValue([]);

                u = new User();
                const expectedMonthlySavedAmount = randomNumberBetween(50, 100);
                Object.defineProperty(u, 'monthlySavedAmount', {get: () => expectedMonthlySavedAmount});

                u.ledger = l;
                u.rentals = r;

                rentalGeneratorPassive = new RentalGenerator(null);
                // @ts-ignore
                rentalGeneratorPassive.getRentals.mockReturnValue([]);

                g = new Generator([{
                    generator: rentalGeneratorPassive,
                    rentalType: RentalTypes.PassiveApartment
                }]);

                date = new Date(Date.now());
            });

            describe('and currentCashFlowByRental is greater than 0', () => {
                test('should add ledger', () => {
                    const expectedMonthly = randomNumberBetween(1, 10);
                    // @ts-ignore
                    r.currentCashFlowByRental.mockReturnValue([({
                        id: '4',
                        cashOnCashPercent: 3,
                        expectedCashOnCash: 2,
                        investmentAmount: 1,

                        cashOnCashAmountAnnual: jest.fn(),
                        cashOnCashAmountMonthly: jest.fn().mockReturnValue(expectedMonthly),
                        minInvestmentAmountByUser: jest.fn(),
                        getExpectedCashOnCashAmountAnnual: jest.fn()
                    } as IRentalCashFlow)]);

                    g.runEntry({today: date, user: u});

                    expect(l.pushLedgerItem).nthCalledWith(1, {
                        amount: u.monthlySavedAmount,
                        type: LedgerItemType.Income,
                        created: date,
                        note: 'saved for month'
                    });

                    expect(l.pushLedgerItem).nthCalledWith(2, new LedgerItem({
                        amount: expectedMonthly,
                        type: LedgerItemType.CashFlow,
                        created: new Date(date.getTime()),
                        note: 'cashflow',
                        referenceId: '4'
                    }));
                });
            });
        });

        describe('and rentals.rentalsForRefinance', () => {
            beforeEach(() => {
                // @ts-ignore
                getRentalsBySort.mockReturnValue([]);

                l = new Ledger();

                r = new RentalCollection();
                // @ts-ignore
                r.rentalsForRefinance.mockReturnValue([]);
                // @ts-ignore
                r.rentalsForSale.mockReturnValue([]);
                // @ts-ignore
                r.currentCashFlowByRental.mockReturnValue([]);

                u = new User();
                const expectedMonthlySavedAmount = randomNumberBetween(50, 100);
                Object.defineProperty(u, 'monthlySavedAmount', {get: () => expectedMonthlySavedAmount});

                u.ledger = l;
                u.rentals = r;

                rentalGeneratorPassive = new RentalGenerator(null);
                // @ts-ignore
                rentalGeneratorPassive.getRentals.mockReturnValue([]);

                g = new Generator([{
                    generator: rentalGeneratorPassive,
                    rentalType: RentalTypes.PassiveApartment
                }]);

                date = new Date(Date.now());
            });

            describe('and currentCashFlowByRental is greater than 0', () => {
                test('should add ledger', () => {
                    const passiveApartment = new PassiveApartment();
                    // @ts-ignore
                    passiveApartment.getRefinanceAmountByDate.mockReturnValue(33);

                    // @ts-ignore
                    r.rentalsForRefinance.mockReturnValue(['4']);
                    // @ts-ignore
                    r.get.mockReturnValue(passiveApartment);

                    g.runEntry({today: date, user: u});

                    expect(l.pushLedgerItem).nthCalledWith(1, {
                        amount: u.monthlySavedAmount,
                        type: LedgerItemType.Income,
                        created: date,
                        note: 'saved for month'
                    });

                    expect(l.pushLedgerItem).nthCalledWith(2, new LedgerItem({
                        amount: 33,
                        type: LedgerItemType.Equity,
                        created: date,
                        note: 'refinance'
                    }));
                });
            });
        });

        describe('and rentals.rentalsForSale', () => {
            beforeEach(() => {
                // @ts-ignore
                getRentalsBySort.mockReturnValue([]);

                l = new Ledger();

                r = new RentalCollection();
                // @ts-ignore
                r.rentalsForRefinance.mockReturnValue([]);

                // @ts-ignore
                r.currentCashFlowByRental.mockReturnValue([]);

                u = new User();

                u.ledger = l;
                u.rentals = r;

                rentalGeneratorPassive = new RentalGenerator(null);
                // @ts-ignore
                rentalGeneratorPassive.getRentals.mockReturnValue([]);

                g = new Generator([{
                    generator: rentalGeneratorPassive,
                    rentalType: RentalTypes.PassiveApartment
                }]);

                date = new Date(Date.now());
            });

            describe('and currentCashFlowByRental is greater than 0', () => {
                test('should add ledger', () => {
                    const passiveApartment: IRentalSale = {
                        equityCapturePercent: 0,
                        expectedSellEquity: 0,
                        id: '211234',
                        minSaleYears: 0,
                        minSellDate: undefined,
                        saleDate: undefined,
                        sellPrice: 0,
                        getSellEquityByDate: jest.fn(),
                        saleEquityByDate: jest.fn(),
                        canSell: jest.fn(),
                        cashOnCashPercent: 1
                    };
                    // @ts-ignore
                    passiveApartment.saleEquityByDate.mockReturnValue(33);

                    // @ts-ignore
                    r.rentalsForSale.mockReturnValue([passiveApartment]);

                    g.runEntry({today: date, user: u});

                    expect(l.pushLedgerItem).nthCalledWith(1, new LedgerItem({
                        amount: 33,
                        type: LedgerItemType.Equity,
                        created: date,
                        note: 'sold',
                        referenceId: '211234'
                    }));
                });
            });
        });

        describe('and getting all rentals', () => {
            let rentalGeneratorSingle;
            beforeEach(() => {
                // @ts-ignore
                getRentalsBySort.mockReturnValue([]);

                l = new Ledger();

                r = new RentalCollection();
                // @ts-ignore
                r.currentCashFlowByRental.mockReturnValue([]);
                // @ts-ignore
                r.rentalsForRefinance.mockReturnValue([]);
                // @ts-ignore
                r.rentalsForSale.mockReturnValue([]);

                u = new User();
                u.ledger = l;
                u.rentals = r;

                rentalGeneratorPassive = new RentalGenerator(null);
                rentalGeneratorSingle = new RentalGenerator(null);

                rentalGeneratorPassive.getRentals.mockReturnValue([{
                    id: '1'
                }]);
                rentalGeneratorSingle.getRentals.mockReturnValue([{
                    id: '2'
                }]);

                // @ts-ignore
                getRentalsBySort.mockReturnValue([{
                    id: '1',
                    canInvestByUser: jest.fn().mockReturnValue(true),
                    purchaseDate: null,
                    investmentAmount: null,
                    minInvestmentAmountByUser: jest.fn().mockReturnValue(10),
                    getRentalType: jest.fn().mockReturnValue(RentalTypes.PassiveApartment)
                }]);

                g = new Generator([{
                    generator: rentalGeneratorSingle,
                    rentalType: RentalTypes.SingleFamily
                }, {
                    generator: rentalGeneratorPassive,
                    rentalType: RentalTypes.PassiveApartment
                }]);

                date = new Date(Date.now());
            });

            describe('and returns rentals', () => {
                test('it should populate from both', () => {
                    const actual = g.runEntry({today: date, user: u});

                    expect(actual.rentals).toEqual([
                        {
                            date, rental: {id: '2'}
                        }, {
                            date, rental: {
                                id: '1',
                                investmentAmount: 10,
                                purchaseDate: date,
                            }
                        }
                    ]);

                    expect(rentalGeneratorPassive.removeRentalById).toBeCalledWith('1', date);
                });
            });
        });
    });

    describe('and simulate', () => {
        let gen: Mocked<Generator>;

        beforeEach(() => {
            l = new Ledger();

            r = new RentalCollection();
            // @ts-ignore
            r.currentCashFlowByRental.mockReturnValue([]);
            // @ts-ignore
            r.rentalsForRefinance.mockReturnValue([]);
            // @ts-ignore
            r.rentalsForSale.mockReturnValue([]);

            u = new User();
            // @ts-ignore
            u.hasMoneyToInvest.mockReturnValue(true);

            u.ledger = l;
            u.rentals = r;

            date = new Date(Date.now());
            gen = new Generator([{
                rentalType: RentalTypes.PassiveApartment,
                generator: new RentalGenerator(null)
            }]) as Mocked<Generator>;

            gen.hasMetGoalOrMaxTime = jest.fn();
            gen.hasMetGoalOrMaxTime.mockReturnValueOnce(false);
            gen.hasMetGoalOrMaxTime.mockReturnValueOnce(false);
            gen.hasMetGoalOrMaxTime.mockReturnValueOnce(true);
            gen.hasMetGoalOrMaxTime.mockReturnValueOnce(false);

            gen.runEntry = jest.fn().mockReturnValueOnce(({
                user: u,
                rentals: [],
                today: date
            } as IRunEntryResult));
            gen.runEntry.mockReturnValueOnce(({
                user: u,
                rentals: [{}],
                today: new Date(date.getUTCFullYear(), date.getUTCMonth() + 1, 1)
            } as IRunEntryResult));
        });

        describe('and looping', () => {
            test('should loop 2 times', async () => {
                const actual = await gen.simulate(u);

                expect(actual).toEqual({
                    user: u,
                    rentals: [{}],
                    date: new Date(date.getUTCFullYear(), date.getUTCMonth() + 2, 1)
                });

                expect(gen.runEntry).toBeCalledTimes(2);

                expect(gen.hasMetGoalOrMaxTime).toBeCalledTimes(3);
            });
        });
    });

    describe('and hasMetGoalOrMaxTime', () => {
        let maxYears;
        let start: Date;
        beforeEach(() => {
            maxYears = 2;
            start = new Date(Date.now());
        });

        describe('and total time is past max years', () => {
            test('should return true', () => {
                const today = new Date(start.getFullYear() + maxYears + 1, start.getUTCMonth(), start.getDate()); /*?*/

                g = new Generator([]);
                expect(g.hasMetGoalOrMaxTime(start, today, null, maxYears)).toBeTruthy();
            });
        });

        describe('and under total time', () => {
            test('should return based on user metMonthlyGoal', () => {
                const today = new Date(start.getFullYear(), start.getUTCMonth(), start.getDate());

                g = new Generator([]);

                const value = randomNumberBetween(0, 2) === 1;

                const usr: IUser = {
                    ledger: undefined,
                    maxInvestAmount: 0,
                    minMonthlyCashOnCashAmountHouse: 0,
                    minMonthlyCashOnCashAmountApartment: 0,
                    minEquityCapturePercent: 0,
                    minInvestAmount: 0,
                    monthlyIncomeAmountGoal: 0,
                    monthlySavedAmount: 0,
                    rentals: undefined,
                    clone: jest.fn(),
                    getLedgerBalance: jest.fn(),
                    hasMoneyToInvest: jest.fn(),
                    metMonthlyGoal: jest.fn().mockReturnValue(value),
                    getAllowedInvestmentLedgerBalance: jest.fn()
                };

                expect(g.hasMetGoalOrMaxTime(start, today, usr, maxYears)).toEqual(value);
            });
        });
    });
});
