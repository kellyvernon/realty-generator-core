'use strict';

jest.mock('../../../src/utils/data-number');
jest.mock('../../../src/rentals/passive-apartment');
jest.mock('../../../src/value-cache');

import {mocked} from '../../helpers/mocker';
import {ValueCache} from '../../../src/value-cache';
import {RentalGenerator} from '../../../src/generators';
import {PassiveApartment} from '../../../src/rentals';
import {randomNumberBetween, fixedAndFloat} from '../../../src/utils';

describe('Rental Generator tests', () => {
    describe('and getRentals', () => {
        describe('and no rentals', () => {
            test('empty returned', () => {
                const valueCache = mocked(new ValueCache(null, null));
                const expectedValue = [];
                valueCache.getValue.mockReturnValue(expectedValue);
                const gen = new RentalGenerator(valueCache);
                expect(gen.getRentals()).toEqual(expectedValue);
            });
        });

        describe('and rental info supplied', () => {
            test('populated returned', () => {
                const maxRentalOpportunities = 5;
                // @ts-ignore
                randomNumberBetween.mockReturnValueOnce(maxRentalOpportunities);

                const expected = [
                    new PassiveApartment(),
                    new PassiveApartment(),
                    new PassiveApartment(),
                    new PassiveApartment(),
                    new PassiveApartment(),
                ];

                const valueCache = mocked(new ValueCache(null, null));
                const ex = [];
                valueCache.getValue.mockReturnValue(ex);
                const gen = new RentalGenerator(valueCache);
                gen.maxRentalOpportunities = maxRentalOpportunities;
                gen.rentalType = PassiveApartment;


                const actual = gen.getRentals();
                expect(JSON.stringify(actual)).toEqual(JSON.stringify(expected));
            });

            test('and in cache', () => {
                const maxRentalOpportunities = 2;

                const expected = [
                    new PassiveApartment(),
                    new PassiveApartment(),
                ];

                const valueCache = mocked(new ValueCache(null, null));
                valueCache.getValue.mockReturnValue(expected);
                const gen = new RentalGenerator(valueCache);
                gen.maxRentalOpportunities = maxRentalOpportunities;

                const actual = gen.getRentals();
                expect(actual).toEqual(expected);
            });

            describe('and rental ranges supplied', () => {
                test('and threshold within range', () => {
                    const maxRentalOpportunities = 2;
                    const cache = mocked(new ValueCache(null, null));
                    cache.getValue.mockReturnValue([]);

                    // totalRandom
                    // @ts-ignore
                    randomNumberBetween.mockReturnValueOnce(1);

                    // purchasePrice
                    // @ts-ignore
                    randomNumberBetween.mockReturnValueOnce(16);
                    // @ts-ignore
                    fixedAndFloat.mockReturnValueOnce(16);

                    // minSaleYears
                    // @ts-ignore
                    randomNumberBetween.mockReturnValueOnce(6);

                    // SellPercent
                    const randomSellPercent = 351;
                    // @ts-ignore
                    randomNumberBetween.mockReturnValueOnce(randomSellPercent);
                    // @ts-ignore
                    fixedAndFloat.mockReturnValueOnce(randomSellPercent);

                    // refinancePercent
                    // @ts-ignore
                    randomNumberBetween.mockReturnValueOnce(10);

                    // minRefinanceMonths
                    // @ts-ignore
                    randomNumberBetween.mockReturnValueOnce(24);

                    const gen = new RentalGenerator(cache);
                    gen.maxRentalOpportunities = maxRentalOpportunities;

                    gen.lowestPriceDown = 200;
                    gen.highestPriceDown = 300;

                    gen.lowestSellPercent = 350;
                    gen.highestSellPercent = 399;

                    gen.lowestMinSellInYears = 5;
                    gen.highestMinSellInYears = 8;

                    gen.lowestRefinancePercent = 20;
                    gen.highestRefinancePercent = 50;

                    gen.lowestMinRefinanceInMonths = 1;
                    gen.highestMinRefinanceInMonths = 4;

                    gen.lowestCashOnCashPercent = 9;
                    gen.highestCashOnCashPercent = 15;

                    gen.rentalType = PassiveApartment;
                    gen.translate = (optsArgs: any) => {
                        const assign = Object.assign({}, optsArgs);
                        // @ts-ignore
                        assign.foo = 'bar';
                        return assign;
                    };

                    gen.getRentals();
                    expect(PassiveApartment).toBeCalledWith({
                        purchaseDate: null,
                        saleDate: null,
                        minSaleYears: 6,
                        purchasePrice: 16,
                        sellPrice: randomSellPercent,
                        refinancePercent: 10,
                        minRefinanceMonths: 24,
                        foo: 'bar'
                    });
                });
            });
        });
    });
});
