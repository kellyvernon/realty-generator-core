'use strict';

jest.mock('../../src/rentals/rental');
import {RentalCollection} from '../../src/rentals/rental-collection';

import {createSandbox} from 'sinon';
import {Rental} from '../../src/rentals';

const sandbox = createSandbox();

const getRental = (): jest.Mocked<Rental> => {
    return new Rental() as jest.Mocked<Rental>;
}

describe('rental collection tests', () => {
    afterEach(() => {
        sandbox.restore();
        jest.restoreAllMocks();
    });

    describe('and rentalsForRefinance', () => {
        it('should return rental', () => {
            const collection = new RentalCollection();

            const expectedRental: jest.Mocked<Rental> = getRental();
            expectedRental.canRefinance.mockReturnValue(true);

            collection.push(expectedRental);

            expect([0]).toEqual(collection.rentalsForRefinance(new Date()));
        });

        it('should not return rental', () => {
            const collection = new RentalCollection();

            const rental: jest.Mocked<Rental> = getRental();
            rental.canRefinance.mockReturnValue(false);

            collection.push(rental);

            expect([]).toEqual(collection.rentalsForRefinance(new Date()));
        });
    });

    describe('and rentalCollectionForSale', () => {
        it('should return rental', () => {
            const collection = new RentalCollection();

            const rental: jest.Mocked<Rental> = getRental();
            rental.canSell.mockReturnValue(false);

            const expectedRental: jest.Mocked<Rental> = getRental();
            expectedRental.canSell.mockReturnValue(true);

            collection.push(rental);
            collection.push(expectedRental);

            expect([1]).toEqual(collection.rentalCollectionForSale(new Date()));
        });

        it('should not return rental', () => {
            const collection = new RentalCollection();

            const rental: jest.Mocked<Rental> = getRental();
            rental.canSell.mockReturnValue(false);

            const rental2: jest.Mocked<Rental> = getRental();
            rental2.canSell.mockReturnValue(false);

            collection.push(rental);
            collection.push(rental2);

            expect([]).toEqual(collection.rentalCollectionForSale(new Date()));
        });
    });

    describe('and getSoldRentals', () => {
        it('should return no rentals', () => {
            const collection = new RentalCollection();

            expect(collection.getSoldRentals().length).toEqual(0);
        });

        it('should return rental', () => {
            const collection = new RentalCollection();

            const rental: jest.Mocked<Rental> = getRental();
            rental.saleDate = new Date();

            const rental2: jest.Mocked<Rental> = getRental();
            rental2.saleDate = null;

            collection.push(rental);
            collection.push(rental2);

            expect(collection.getSoldRentals().length).toEqual(1);
        });

        it('should not return rental', () => {
            const collection = new RentalCollection();

            const rental1: jest.Mocked<Rental> = getRental();
            rental1.saleDate = new Date();

            const rental2: jest.Mocked<Rental> = getRental();
            rental2.saleDate = new Date();

            collection.push(rental1);
            collection.push(rental2);

            expect(collection.getSoldRentals().length).toEqual(2);
        });
    });

    describe('and isPopulated', () => {
        it('is empty', () => {
            const r = new RentalCollection();
            expect(r.isPopulated()).toBeFalsy();
        });
        it('and contains item', () => {
            const r = new RentalCollection();
            r.push(getRental());
            expect(r.isPopulated()).toBeTruthy();
        });
        it('and collection is null', () => {
            const r = new RentalCollection();
            expect(r.isPopulated()).toBeFalsy();
        });
    });

    describe('and last', () => {
        it('is empty', () => {
            const r = new RentalCollection();
            expect(r.last()).toBeNull();
        });
        it('and contains item', () => {
            const r = new RentalCollection();
            const expected = getRental();
            r.push(expected);
            expect(r.last()).toEqual(expected);
        });
    });

    describe('and totalYears', () => {
        it('is empty', () => {
            const r = new RentalCollection();
            expect(r.totalYears()).toBe(-1);
        });

        it('and contains item', () => {
            const r = new RentalCollection();
            const date = new Date(Date.now());
            const date2 = new Date(Date.now());

            const rental1 = getRental();
            rental1.purchaseDate = date;

            r.push(rental1);

            const numberOfYears = 3;
            date2.setFullYear(date2.getFullYear() + numberOfYears);

            const rental2 = getRental();
            rental2.purchaseDate = date2;

            r.push(rental2);

            expect(r.totalYears()).toEqual(numberOfYears);
        });
    });

    describe('and totalMonths', () => {
        it('is empty', () => {
            const r = new RentalCollection();
            expect(r.totalMonths()).toBe(-1);
        });

        it('and contains item', () => {
            const r = new RentalCollection();
            const date = new Date(Date.now());
            const date2 = new Date(Date.now());

            const rental1 = getRental();
            rental1.purchaseDate = date;

            r.push(rental1);

            const numberOfYears = 3;
            date2.setFullYear(date2.getFullYear() + numberOfYears);

            const rental2 = getRental();
            rental2.purchaseDate = date2;

            r.push(rental2);
            expect(r.totalMonths()).toEqual(numberOfYears * 12);
        });
    });

    describe('and rentalsForSale', () => {
        it('should return empty collection', () => {
            const r = new RentalCollection();

            expect(r.rentalsForSale(new Date())).toEqual([]);
        });
        describe('and no rentals for sale', () => {
            it('should return empty collection', () => {
                const r = new RentalCollection();

                const rental1 = getRental();
                rental1.canSell.mockReturnValue(false);

                r.push(rental1);

                expect(r.rentalsForSale(new Date())).toEqual([]);
            });
        });
        describe('and rentals for sale', () => {
            it('should return empty collection', () => {
                const r = new RentalCollection();

                const rental1 = getRental();
                rental1.canSell.mockReturnValue(false);

                r.push(rental1);

                const expectedRental = getRental();
                expectedRental.canSell.mockReturnValue(true);

                r.push(expectedRental);

                expect(r.rentalsForSale(new Date())).toEqual([expectedRental]);
            });
        });
    });

    describe('and currentCashFlowByRental', () => {
        it('should return 0', () => {
            const collection = new RentalCollection();

            expect([]).toEqual(collection.currentCashFlowByRental(new Date()));
        });

        it('should return collection with cashFlow', () => {
            const collection = new RentalCollection();

            const expectedRental = getRental();
            expectedRental.cashOnCashAmountMonthly.mockReturnValue(3);

            collection.push(expectedRental);

            expect([expectedRental]).toEqual(collection.currentCashFlowByRental(new Date()));
        });

        it('should not return collection with cashFlow', () => {
            const collection = new RentalCollection();

            const rental = getRental();
            rental.cashOnCashAmountMonthly.mockReturnValue(0);

            collection.push(rental);

            expect([]).toEqual(collection.currentCashFlowByRental(new Date()));
        });
    });

    describe('and cashOnCashAmountMonthly', () => {
        it('should return collection with cashFlow', () => {
            const collection = new RentalCollection();

            expect(0).toEqual(collection.cashOnCashAmountMonthly(new Date()));
        });

        it('should return collection with cashFlow', () => {
            const collection = new RentalCollection();

            const expectedRental = getRental();
            expectedRental.cashOnCashAmountMonthly.mockReturnValue(3);

            collection.push(expectedRental);

            expect(3).toEqual(collection.cashOnCashAmountMonthly(new Date()));
        });

        it('should not return collection with cashFlow', () => {
            const collection = new RentalCollection();

            const rental = getRental();
            rental.cashOnCashAmountMonthly.mockReturnValue(0);

            collection.push(rental);

            expect(0).toEqual(collection.cashOnCashAmountMonthly(new Date()));
        });
    });
});
