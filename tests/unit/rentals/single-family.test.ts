'use strict';

jest.mock('../../../src/user');
import {mocked} from '../../helpers/mocker';

import {IUser, User} from '../../../src/user';
import {InvestmentReasons, RentalTypes, SingleFamily, UserInvestResult} from '../../../src/rentals';

describe('single family tests', () => {
    const expectedCashOnCashAmountMonthly = 10;
    const investmentAmount = 1000;
    const expectedInvestmentPercent = 25.00;
    const expectedPurchasePrice = investmentAmount * (expectedInvestmentPercent / 100);
    const expectedSellPrice = expectedPurchasePrice * 10;

    const purchaseDate = new Date(Date.now());
    purchaseDate.setHours(0);
    purchaseDate.setMinutes(0);
    purchaseDate.setSeconds(0);
    purchaseDate.setMilliseconds(0);

    let rental: SingleFamily;

    beforeEach(() => {
        rental = new SingleFamily({
            investmentPercent: expectedInvestmentPercent,
            purchasePrice: expectedPurchasePrice,
            sellPrice: expectedSellPrice,
            cashOnCashAmountMonthly: expectedCashOnCashAmountMonthly,
            purchaseDate,
            minSaleYears: 5,
            refinancePercent: 0,
            minRefinanceMonths: 0
        });
    });

    test('cashOnCashPercent should match ', () => {
        expect(rental.cashOnCashPercent).toEqual(expectedCashOnCashAmountMonthly * 12 / expectedPurchasePrice * 100);
    });

    describe('and cashOnCashAmountAnnual', () => {
        test('before maturity', () => {
            const futureTodayDate = new Date(
                purchaseDate.getFullYear(),
                purchaseDate.getMonth() + 1,
                purchaseDate.getDate());
            rental.investmentAmount = investmentAmount;
            expect(rental.cashOnCashAmountAnnual(futureTodayDate)).toEqual(expectedCashOnCashAmountMonthly * 12 / 2);
        });

        test('after maturity', () => {
            const futureTodayDate = new Date(
                purchaseDate.getFullYear() + 1,
                purchaseDate.getMonth(),
                purchaseDate.getDate());
            rental.investmentAmount = investmentAmount;
            expect(rental.cashOnCashAmountAnnual(futureTodayDate)).toEqual(expectedCashOnCashAmountMonthly * 12);
        });

        test('after sale', () => {
            const futureTodayDate = new Date(
                purchaseDate.getFullYear() + 1,
                purchaseDate.getMonth(),
                purchaseDate.getDate());

            rental.saleDate = purchaseDate;
            expect(rental.cashOnCashAmountAnnual(futureTodayDate)).toEqual(0);
        });
    });

    describe('and cashOnCashAmountMonthly', () => {
        test('before maturity', () => {
            const futureTodayDate = new Date(
                purchaseDate.getFullYear(),
                purchaseDate.getMonth() + 1,
                purchaseDate.getDate());
            rental.investmentAmount = investmentAmount;
            expect(rental.cashOnCashAmountMonthly(futureTodayDate)).toEqual(expectedCashOnCashAmountMonthly / 2);
        });

        test('after maturity', () => {
            const futureTodayDate = new Date(
                purchaseDate.getFullYear() + 1,
                purchaseDate.getMonth(),
                purchaseDate.getDate());
            rental.investmentAmount = investmentAmount;
            expect(rental.cashOnCashAmountMonthly(futureTodayDate)).toEqual(expectedCashOnCashAmountMonthly);
        });

        test('after sale', () => {
            const futureTodayDate = new Date(
                purchaseDate.getFullYear() + 1,
                purchaseDate.getMonth(),
                purchaseDate.getDate());

            rental.saleDate = purchaseDate;
            expect(rental.cashOnCashAmountMonthly(futureTodayDate)).toEqual(0);
        });
    });

    describe('and canSell', () => {
        test('before purchase', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear(),
                rental.minSellDate.getMonth() - 1,
                rental.minSellDate.getDate());

            expect(rental.canSell(futureTodayDate)).toBeFalsy();
        });

        test('before sell date', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear(),
                rental.minSellDate.getMonth() - 1,
                rental.minSellDate.getDate());

            expect(rental.canSell(futureTodayDate)).toBeFalsy();
        });

        test('after purchase and after sell date', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear() + 1,
                rental.minSellDate.getMonth(),
                rental.minSellDate.getDate());

            expect(rental.canSell(futureTodayDate)).toBeTruthy();
        });

        test('after purchase and before min sell date', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear() - 1,
                rental.minSellDate.getMonth() - 1,
                rental.minSellDate.getDate());

            expect(rental.canSell(futureTodayDate)).toBeFalsy();
        });

        test('after sale', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear() + 1,
                rental.minSellDate.getMonth(),
                rental.minSellDate.getDate());

            rental.saleDate = purchaseDate;
            expect(rental.canSell(futureTodayDate)).toBeFalsy();
        });
    });

    describe('and getSellEquityByDate', () => {
        test('before purchase', () => {
            const futureTodayDate = new Date(
                purchaseDate.getFullYear(),
                purchaseDate.getMonth() - 1,
                purchaseDate.getDate());

            expect(rental.getSellEquityByDate(futureTodayDate)).toBe(0);
        });

        test('before sale date', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear(),
                rental.minSellDate.getMonth() - 1,
                rental.minSellDate.getDate());

            expect(rental.getSellEquityByDate(futureTodayDate)).toBe(0);
        });

        test('after purchase and after sell date', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear() + 3,
                rental.minSellDate.getMonth() + 1,
                rental.minSellDate.getDate());

            rental.investmentAmount = investmentAmount;
            const expectedAmount = expectedSellPrice - expectedPurchasePrice - investmentAmount;
            expect(rental.getSellEquityByDate(futureTodayDate)).toBe(expectedAmount);
        });

        test('after purchase and after actual sale date', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear() + 3,
                rental.minSellDate.getMonth() + 1,
                rental.minSellDate.getDate());

            rental.saleDate = rental.minSellDate;

            expect(rental.getSellEquityByDate(futureTodayDate)).toBe(0);
        });

        test('after purchase and before sale date', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear() - 1,
                rental.minSellDate.getMonth() - 1,
                rental.minSellDate.getDate());

            expect(rental.getSellEquityByDate(futureTodayDate)).toBe(0);
        });
    });

    describe('and minInvestmentAmountByUser', () => {
        test('and under should return lowest', () => {
            expect(rental.minInvestmentAmountByUser(null)).toEqual(expectedInvestmentPercent * expectedPurchasePrice / 100);
        });
    });

    describe('and canInvestByUser', () => {
        describe('and user willBeAbleToInvest true', () => {
            test('and total below minInvestmentAmountByUser', () => {
                const user: IUser = {
                    ledger: undefined,
                    maxInvestAmount: 0,
                    minEquityCapturePercent: 0,
                    minInvestAmount: 0,
                    minMonthlyCashOnCashAmountApartment: 0,
                    monthlyIncomeAmountGoal: 0,
                    monthlySavedAmount: 0,
                    rentals: undefined,
                    clone: jest.fn(),
                    getAllowedInvestmentLedgerBalance: jest.fn(),
                    metMonthlyGoal: jest.fn(),
                    minMonthlyCashOnCashAmountHouse: 20,
                    hasMoneyToInvest: jest.fn().mockReturnValue(true),
                    getLedgerBalance: jest.fn().mockReturnValue(0)
                };

                expect(rental.canInvestByUser(user)).toEqual(expect.objectContaining({
                    canInvest: false,
                    investmentReason: InvestmentReasons.DoesNotMeetUserRuleCashOnCash
                }));
                expect(user.getAllowedInvestmentLedgerBalance).toBeCalled();
            });

            describe('and total not below minInvestmentAmountByUser', () => {
                describe('and user minEquityCapturePercent expectations are higher', () => {
                    test('canInvestByUser should be false', () => {
                        const user: jest.Mocked<IUser> = mocked({
                            getLedgerBalance: jest.fn(),
                            minEquityCapturePercent: rental.equityCapturePercent + 1,
                            minMonthlyCashOnCashAmountHouse: 0,
                            minMonthlyCashOnCashAmountApartment: 0,
                            ledger: null,
                            maxInvestAmount: 0,
                            minInvestAmount: 0,
                            monthlyIncomeAmountGoal: 0,
                            monthlySavedAmount: 0,
                            rentals: null,
                            metMonthlyGoal: jest.fn(),
                            hasMoneyToInvest: jest.fn().mockReturnValue(true),
                            clone: jest.fn(),
                            getAllowedInvestmentLedgerBalance: jest.fn()
                        });
                        user.getAllowedInvestmentLedgerBalance.mockReturnValue(rental.minInvestmentAmountByUser(user));

                        expect(rental.canInvestByUser(user)).toEqual(expect.objectContaining({
                            canInvest: false,
                            investmentReason: InvestmentReasons.DoesNotMeetUserRuleEquityCapture
                        }));
                    });
                });

                describe('and user minEquityCapturePercent expectations are same', () => {
                    describe('and user minMonthlyCashOnCashAmountHouse expectations are higher', () => {
                        test('canInvestByUser should be false', () => {
                            const user: jest.Mocked<IUser> = mocked({
                                getLedgerBalance: jest.fn(),
                                minEquityCapturePercent: rental.equityCapturePercent,
                                minMonthlyCashOnCashAmountHouse: rental.cashOnCashPercent + 1,
                                minMonthlyCashOnCashAmountApartment: 0,
                                ledger: null,
                                maxInvestAmount: 0,
                                minInvestAmount: 0,
                                monthlyIncomeAmountGoal: 0,
                                monthlySavedAmount: 0,
                                rentals: null,
                                metMonthlyGoal: jest.fn(),
                                hasMoneyToInvest: jest.fn().mockReturnValue(true),
                                clone: jest.fn(),
                                getAllowedInvestmentLedgerBalance: jest.fn()
                            });
                            user.getAllowedInvestmentLedgerBalance.mockReturnValue(rental.minInvestmentAmountByUser(user));

                            expect(rental.canInvestByUser(user)).toEqual(expect.objectContaining({
                                canInvest: false,
                                investmentReason: InvestmentReasons.DoesNotMeetUserRuleCashOnCash
                            }));
                        });
                    });

                    describe('and user minMonthlyCashOnCashAmountHouse expectations are same', () => {
                        test('canInvestByUser should be true', () => {
                            const user: jest.Mocked<IUser> = mocked({
                                getLedgerBalance: jest.fn(),
                                minEquityCapturePercent: rental.equityCapturePercent,
                                minMonthlyCashOnCashAmountHouse: expectedCashOnCashAmountMonthly,
                                minMonthlyCashOnCashAmountApartment: 0,
                                ledger: null,
                                maxInvestAmount: 0,
                                minInvestAmount: 0,
                                monthlyIncomeAmountGoal: 0,
                                monthlySavedAmount: 0,
                                rentals: null,
                                hasMoneyToInvest: jest.fn().mockReturnValue(true),
                                metMonthlyGoal: jest.fn(),
                                clone: jest.fn(),
                                getAllowedInvestmentLedgerBalance: jest.fn()
                            });
                            user.getAllowedInvestmentLedgerBalance.mockReturnValue(rental.minInvestmentAmountByUser(user));
                            expect(rental.canInvestByUser(user)).toEqual(new UserInvestResult(true, InvestmentReasons.CanInvest));
                        });
                    });
                });
            });
        });

        describe('and user hasMoneyToInvest false', () => {
            test('and above', () => {
                const user = mocked(new User());
                user.hasMoneyToInvest.mockReturnValue(false)
                expect(rental.canInvestByUser(user)).toEqual(new UserInvestResult(false, InvestmentReasons.UserHasNoMoneyToInvest));
                expect(user.getLedgerBalance).not.toBeCalled();
            });
        });
    });

    describe('and getRentalType', () => {
        test('should be matching', () => {
            expect(rental.getRentalType()).toEqual(RentalTypes.SingleFamily);
        });
    });
});
