'use strict';

import {InvestmentReasons, UserInvestResult} from '../../../src/rentals';

jest.mock('../../../src/user');

import {mocked} from '../../helpers/mocker';
import {IRental, Rental} from '../../../src/rentals/rental';
import {User} from '../../../src/user';

class ImpRental extends Rental {
    constructor(o) {
        super(o);
    }
}

describe('rental tests', () => {
    const investmentAmount = 1000;
    const expectedPurchasePrice = investmentAmount * 10;
    const expectedSellPrice = expectedPurchasePrice * 10;
    const expectedRefinancePercent = 50;

    const purchaseDate = new Date(Date.now());
    purchaseDate.setHours(0);
    purchaseDate.setMinutes(0);
    purchaseDate.setSeconds(0);
    purchaseDate.setMilliseconds(0);


    let rental: IRental;

    beforeEach(() => {
        rental = new ImpRental({
            purchasePrice: expectedPurchasePrice,
            sellPrice: expectedSellPrice,
            purchaseDate,
            minSaleYears: 5,
            refinancePercent: expectedRefinancePercent,
            minRefinanceMonths: 36,
        });
    });

    test('and id populated', () => {
        expect(rental.id).not.toBeNull();
    });

    describe('and newed', () => {
        test('should throw', () => {
            expect(() => {
                // tslint:disable-next-line:no-unused-expression
                new Rental({});
            }).toThrow('abstract');
        });
    });

    describe('and cashOnCashPercent', () => {
        test('default value', () => {
            expect(rental.cashOnCashPercent).toEqual(0);
        });
    });

    describe('and equityCapturePercent', () => {
        test('default value', () => {
            const rentalSellPriceZero = new ImpRental({
                purchasePrice: expectedPurchasePrice,
                sellPrice: 0,
                purchaseDate,
                minSaleYears: 5,
                refinancePercent: expectedRefinancePercent,
                minRefinanceMonths: 36,
            });
            expect(rentalSellPriceZero.equityCapturePercent).toEqual(0);
        });

        test('should match', () => {
            const i = new ImpRental({
                sellPrice: 100,
                purchasePrice: 50
            });

            const capturePercent = i.equityCapturePercent;
            expect(capturePercent).toEqual(200);
        });
    });

    describe('and cashOnCashAmountAnnual', () => {
        test('default value', () => {
            expect(rental.cashOnCashAmountAnnual(null)).toEqual(0);
        });
    });

    describe('and cashOnCashAmountMonthly', () => {
        test('default value', () => {
            expect(rental.cashOnCashAmountMonthly(null)).toEqual(0);
        });
    });

    describe('and canSell', () => {
        test('before purchase', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear(),
                rental.minSellDate.getMonth() - 1,
                rental.minSellDate.getDate());

            expect(rental.canSell(futureTodayDate)).toBeFalsy();
        });

        test('before sell date', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear(),
                rental.minSellDate.getMonth() - 1,
                rental.minSellDate.getDate());

            expect(rental.canSell(futureTodayDate)).toBeFalsy();
        });

        test('after purchase and after sell date', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear() + 1,
                rental.minSellDate.getMonth(),
                rental.minSellDate.getDate());

            expect(rental.canSell(futureTodayDate)).toBeTruthy();
        });

        test('after purchase and before min sell date', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear() - 1,
                rental.minSellDate.getMonth() - 1,
                rental.minSellDate.getDate());

            expect(rental.canSell(futureTodayDate)).toBeFalsy();
        });

        test('after sale', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear() + 1,
                rental.minSellDate.getMonth(),
                rental.minSellDate.getDate());

            rental.saleDate = purchaseDate;
            expect(rental.canSell(futureTodayDate)).toBeFalsy();
        });
    });

    describe('and getSellEquityByDate', () => {
        test('before purchase', () => {
            const futureTodayDate = new Date(
                purchaseDate.getFullYear(),
                purchaseDate.getMonth() - 1,
                purchaseDate.getDate());

            expect(rental.getSellEquityByDate(futureTodayDate)).toBe(0);
        });

        test('before sale date', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear(),
                rental.minSellDate.getMonth() - 1,
                rental.minSellDate.getDate());

            expect(rental.getSellEquityByDate(futureTodayDate)).toBe(0);
        });

        test('after purchase and after sell date', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear() + 3,
                rental.minSellDate.getMonth() + 1,
                rental.minSellDate.getDate());

            const investmentPercent = investmentAmount / expectedPurchasePrice;
            rental.investmentAmount = investmentAmount;
            expect(rental.getSellEquityByDate(futureTodayDate)).toBe((expectedSellPrice - expectedPurchasePrice) * investmentPercent);
        });

        test('after purchase and after actual sale date', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear() + 3,
                rental.minSellDate.getMonth() + 1,
                rental.minSellDate.getDate());

            rental.saleDate = rental.minSellDate;

            expect(rental.getSellEquityByDate(futureTodayDate)).toBe(0);
        });

        test('after purchase and before sale date', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear() - 1,
                rental.minSellDate.getMonth() - 1,
                rental.minSellDate.getDate());

            expect(rental.getSellEquityByDate(futureTodayDate)).toBe(0);
        });
    });

    describe('and minInvestmentAmountByUser', () => {
        test('should return 0', () => {
            const u = mocked(new User());
            u.getLedgerBalance.mockReturnValue(0);
            expect(rental.minInvestmentAmountByUser(u)).toEqual(0);
        });
    });

    describe('and canInvestByUser', () => {
        test('should be false', () => {
            const user = new User();
            expect(rental.canInvestByUser(user)).toEqual(new UserInvestResult(false, InvestmentReasons.Unknown));
        });
    });

    describe('and getRefinanceAmountByDate', () => {
        test('before purchase', () => {
            const futureTodayDate = new Date(
                purchaseDate.getFullYear(),
                purchaseDate.getMonth() - 1,
                purchaseDate.getDate());

            expect(rental.getRefinanceAmountByDate(futureTodayDate)).toBe(0);
        });

        test('before refinance date', () => {
            const futureTodayDate = new Date(
                rental.minRefinanceDate.getFullYear(),
                rental.minRefinanceDate.getMonth() - 1,
                rental.minRefinanceDate.getDate());

            expect(rental.getRefinanceAmountByDate(futureTodayDate)).toBe(0);
        });

        test('after purchase and after refinance date', () => {
            const futureTodayDate = new Date(
                rental.minRefinanceDate.getFullYear() + 3,
                rental.minRefinanceDate.getMonth() + 1,
                rental.minRefinanceDate.getDate());

            const expectedRefiAmount = investmentAmount * (expectedRefinancePercent / 100);
            rental.investmentAmount = investmentAmount;
            expect(rental.getRefinanceAmountByDate(futureTodayDate)).toBe(expectedRefiAmount);
        });

        test('after purchase and after actual refinance date', () => {
            const futureTodayDate = new Date(
                rental.minRefinanceDate.getFullYear() + 3,
                rental.minRefinanceDate.getMonth() + 1,
                rental.minRefinanceDate.getDate());

            rental.investmentAmount = investmentAmount;
            rental.refinanceDate = rental.minRefinanceDate;

            expect(rental.getRefinanceAmountByDate(futureTodayDate)).toBe(0);
        });

        test('after purchase and before refinance date', () => {
            const futureTodayDate = new Date(
                rental.minRefinanceDate.getFullYear() - 1,
                rental.minRefinanceDate.getMonth() - 1,
                rental.minRefinanceDate.getDate());

            expect(rental.getRefinanceAmountByDate(futureTodayDate)).toBe(0);
        });

        test('after sale', () => {
            const futureTodayDate = new Date(
                purchaseDate.getFullYear() + 1,
                purchaseDate.getMonth(),
                purchaseDate.getDate());

            rental.saleDate = purchaseDate;
            expect(rental.canRefinance(futureTodayDate)).toBeFalsy();
        });
    });

    describe('and get minRefinanceDate', () => {
        test('should align', () => {
            const month = new Date(rental.purchaseDate.getFullYear(), rental.purchaseDate.getMonth(), 1);
            month.setMonth(month.getMonth() + 36);

            expect(rental.minRefinanceDate).toEqual(month)
        })
    })
});
