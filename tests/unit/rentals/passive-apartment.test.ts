'use strict';

jest.mock('../../../src/user');

import {mocked} from '../../helpers/mocker';
import {IUser, User} from '../../../src/user';

import {InvestmentReasons, PassiveApartment, RentalTypes, UserInvestResult} from '../../../src/rentals';

describe('passive-apartment tests', () => {
    const expectedCashOnCashPercent = 10;
    const investmentAmount = 1000;
    const expectedCashOnCashAmount = 100;
    const expectedPurchasePrice = investmentAmount * 10;
    const expectedSellPrice = expectedPurchasePrice * 10;
    const expectedInvestmentAmounts = [100, 500, 1000];

    const purchaseDate = new Date(Date.now());
    purchaseDate.setHours(0);
    purchaseDate.setMinutes(0);
    purchaseDate.setSeconds(0);
    purchaseDate.setMilliseconds(0);

    /**
     * @type {PassiveApartment}
     */
    let rental;

    beforeEach(() => {
        rental = new PassiveApartment({
            investmentAmounts: expectedInvestmentAmounts,
            purchasePrice: expectedPurchasePrice,
            sellPrice: expectedSellPrice,
            cashOnCashPercent: expectedCashOnCashPercent,
            purchaseDate,
            minSaleYears: 5,
            minRefinanceMonths: 0,
            refinancePercent: 0
        });
    });

    afterEach(() => {
        jest.restoreAllMocks();
    });

    test('cashOnCashPercent should match ', () => {
        expect(rental.cashOnCashPercent).toEqual(expectedCashOnCashPercent);
    });

    describe('and cashOnCashAmountAnnual', () => {
        test('before maturity', () => {
            const futureTodayDate = new Date(
                purchaseDate.getFullYear(),
                purchaseDate.getMonth() + 1,
                purchaseDate.getDate());
            rental.investmentAmount = investmentAmount;
            expect(rental.cashOnCashAmountAnnual(futureTodayDate)).toEqual(expectedCashOnCashAmount / 2);
        });

        test('after maturity', () => {
            const futureTodayDate = new Date(
                purchaseDate.getFullYear() + 1,
                purchaseDate.getMonth(),
                purchaseDate.getDate());
            rental.investmentAmount = investmentAmount;
            expect(rental.cashOnCashAmountAnnual(futureTodayDate)).toEqual(expectedCashOnCashAmount);
        });

        test('after sale', () => {
            const futureTodayDate = new Date(
                purchaseDate.getFullYear() + 1,
                purchaseDate.getMonth(),
                purchaseDate.getDate());

            rental.saleDate = purchaseDate;
            expect(rental.cashOnCashAmountAnnual(futureTodayDate)).toEqual(0);
        });

        test('and example', () => {
            const example = new PassiveApartment({
                purchasePrice: 17500000,
                sellPrice: expectedSellPrice,
                cashOnCashPercent: 7,
                purchaseDate,
                minRefinanceMonths: 36,
                refinancePercent: 0,
                investmentAmounts: [],
                minSaleYears: 0
            });
            example.investmentAmount = 100000;
            const actual = example.cashOnCashAmountMonthly(new Date(purchaseDate.getUTCFullYear() + 3, purchaseDate.getUTCMonth(), purchaseDate.getUTCDate()));
            const quarterlyCashFlow = actual * 3;
            expect(quarterlyCashFlow).toBeGreaterThan(1740);
            expect(quarterlyCashFlow).toBeLessThan(1750);
        });

    });

    describe('and canRefinance', () => {
        test('before purchase', () => {
            const futureTodayDate = new Date(
                purchaseDate.getFullYear(),
                purchaseDate.getMonth() - 1,
                purchaseDate.getDate());

            expect(rental.canRefinance(futureTodayDate)).toBeFalsy();
        });

        test('before refinance date', () => {
            const futureTodayDate = new Date(
                rental.minRefinanceDate.getFullYear(),
                rental.minRefinanceDate.getMonth() - 1,
                rental.minRefinanceDate.getDate());

            expect(rental.canRefinance(futureTodayDate)).toBeFalsy();
        });

        test('after purchase and after refinance date', () => {
            const futureTodayDate = new Date(
                purchaseDate.getFullYear() + 3,
                purchaseDate.getMonth() + 1,
                purchaseDate.getDate());

            expect(rental.canRefinance(futureTodayDate)).toBeTruthy();
        });

        test('after purchase and before refinance date', () => {
            const futureTodayDate = new Date(
                rental.minRefinanceDate.getFullYear() - 1,
                rental.minRefinanceDate.getMonth() - 1,
                rental.minRefinanceDate.getDate());

            expect(rental.canRefinance(futureTodayDate)).toBeFalsy();
        });

        test('after sale', () => {
            const futureTodayDate = new Date(
                purchaseDate.getFullYear() + 1,
                purchaseDate.getMonth(),
                purchaseDate.getDate());

            rental.saleDate = purchaseDate;
            expect(rental.canRefinance(futureTodayDate)).toBeFalsy();
        });
    });

    describe('and canSell', () => {
        test('before purchase', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear(),
                rental.minSellDate.getMonth() - 1,
                rental.minSellDate.getDate());

            expect(rental.canSell(futureTodayDate)).toBeFalsy();
        });

        test('before sell date', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear(),
                rental.minSellDate.getMonth() - 1,
                rental.minSellDate.getDate());

            expect(rental.canSell(futureTodayDate)).toBeFalsy();
        });

        test('after purchase and after sell date', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear() + 1,
                rental.minSellDate.getMonth(),
                rental.minSellDate.getDate());

            expect(rental.canSell(futureTodayDate)).toBeTruthy();
        });

        test('after purchase and before min sell date', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear() - 1,
                rental.minSellDate.getMonth() - 1,
                rental.minSellDate.getDate());

            expect(rental.canSell(futureTodayDate)).toBeFalsy();
        });

        test('after sale', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear() + 1,
                rental.minSellDate.getMonth(),
                rental.minSellDate.getDate());

            rental.saleDate = purchaseDate;
            expect(rental.canSell(futureTodayDate)).toBeFalsy();
        });
    });

    describe('and getSellEquityByDate', () => {
        test('before purchase', () => {
            const futureTodayDate = new Date(
                purchaseDate.getFullYear(),
                purchaseDate.getMonth() - 1,
                purchaseDate.getDate());

            expect(rental.getSellEquityByDate(futureTodayDate)).toBe(0);
        });

        test('before sale date', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear(),
                rental.minSellDate.getMonth() - 1,
                rental.minSellDate.getDate());

            expect(rental.getSellEquityByDate(futureTodayDate)).toBe(0);
        });

        test('after purchase and after sell date', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear() + 3,
                rental.minSellDate.getMonth() + 1,
                rental.minSellDate.getDate());

            const investmentPercent = investmentAmount / expectedPurchasePrice;
            rental.investmentAmount = investmentAmount;
            expect(rental.getSellEquityByDate(futureTodayDate)).toBe((expectedSellPrice - expectedPurchasePrice) * investmentPercent);
        });

        test('after purchase and after actual sale date', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear() + 3,
                rental.minSellDate.getMonth() + 1,
                rental.minSellDate.getDate());

            rental.saleDate = rental.minSellDate;

            expect(rental.getSellEquityByDate(futureTodayDate)).toBe(0);
        });

        test('after purchase and before sale date', () => {
            const futureTodayDate = new Date(
                rental.minSellDate.getFullYear() - 1,
                rental.minSellDate.getMonth() - 1,
                rental.minSellDate.getDate());

            expect(rental.getSellEquityByDate(futureTodayDate)).toBe(0);
        });

        test('after sale', () => {
            const futureTodayDate = new Date(
                purchaseDate.getFullYear() + 1,
                purchaseDate.getMonth(),
                purchaseDate.getDate());

            rental.saleDate = purchaseDate;
            expect(rental.canRefinance(futureTodayDate)).toBeFalsy();
        });
    });

    describe('and minInvestmentAmountByUser', () => {
        test('and under should return lowest', () => {
            const u = new User();
            // @ts-ignore
            u.getLedgerBalance.mockReturnValue(0);
            expect(rental.minInvestmentAmountByUser(u)).toEqual(expectedInvestmentAmounts[0]);
        });

        test('and over min should return lowest', () => {
            const u = new User();
            // @ts-ignore
            u.getLedgerBalance.mockReturnValue(1);
            expect(rental.minInvestmentAmountByUser(u)).toEqual(expectedInvestmentAmounts[0]);
        });

        test('and over highest should return highest', () => {
            const amount = expectedInvestmentAmounts[2] + 3;
            const u = new User();
            // @ts-ignore
            u.getLedgerBalance.mockReturnValue(amount);
            expect(rental.minInvestmentAmountByUser(u)).toEqual(expectedInvestmentAmounts[2]);
        });

        test('should return closest', () => {
            const amount = expectedInvestmentAmounts[1] + 2;
            const u = new User();
            // @ts-ignore
            u.getLedgerBalance.mockReturnValue(amount);
            expect(rental.minInvestmentAmountByUser(u)).toEqual(expectedInvestmentAmounts[1]);
        });

        test('should return closest', () => {
            const amount = expectedInvestmentAmounts[2] - 10;
            const u = new User();
            // @ts-ignore
            u.getLedgerBalance.mockReturnValue(amount);
            expect(rental.minInvestmentAmountByUser(u)).toEqual(expectedInvestmentAmounts[1]);
        });

        test('should return closest', () => {
            const amount = expectedInvestmentAmounts[0] + 1;
            const u = new User();
            // @ts-ignore
            u.getLedgerBalance.mockReturnValue(amount);
            expect(rental.minInvestmentAmountByUser(u)).toEqual(expectedInvestmentAmounts[0]);
        });
    });

    describe('and canInvestByUser', () => {
        describe('and user willBeAbleToInvest true', () => {
            test('and getLedgerTotal below', () => {
                const user: jest.Mocked<IUser> = mocked({
                    getLedgerBalance: jest.fn().mockReturnValue(0),
                    minEquityCapturePercent: expectedCashOnCashPercent / 2,
                    minMonthlyCashOnCashAmountHouse: 0,
                    minMonthlyCashOnCashAmountApartment: expectedCashOnCashPercent / 2,
                    rentals: null,
                    monthlySavedAmount: 0,
                    monthlyIncomeAmountGoal: 0,
                    minInvestAmount: 0,
                    maxInvestAmount: 0,
                    ledger: null,
                    metMonthlyGoal: jest.fn(),
                    hasMoneyToInvest: jest.fn().mockReturnValue(true),
                    clone: jest.fn(),
                    getAllowedInvestmentLedgerBalance: jest.fn()
                });

                expect(rental.canInvestByUser(user)).toEqual(expect.objectContaining({
                    canInvest: false,
                    investmentReason: InvestmentReasons.UserHasNotSavedEnoughMoney
                }));
            });

            test('and getLedgerTotal, minMonthlyCashOnCashAmountApartment, and minEquityCapturePercent above', () => {
                const user: jest.Mocked<IUser> = mocked({
                    getLedgerBalance: jest.fn().mockReturnValue(100000),
                    minEquityCapturePercent: expectedCashOnCashPercent / 2,
                    minMonthlyCashOnCashAmountHouse: 0,
                    minMonthlyCashOnCashAmountApartment: expectedCashOnCashPercent / 2,
                    rentals: null,
                    monthlySavedAmount: 0,
                    monthlyIncomeAmountGoal: 0,
                    minInvestAmount: 0,
                    maxInvestAmount: 0,
                    ledger: null,
                    metMonthlyGoal: jest.fn(),
                    hasMoneyToInvest: jest.fn().mockReturnValue(true),
                    clone: jest.fn(),
                    getAllowedInvestmentLedgerBalance: jest.fn()
                });
                expect(rental.canInvestByUser(user)).toEqual(new UserInvestResult(true, InvestmentReasons.CanInvest));
                expect(user.hasMoneyToInvest).toBeCalled();
                expect(user.getLedgerBalance).toBeCalled();
            });

            test('and getLedgerTotal, minMonthlyCashOnCashAmountApartment above, while minEquityCapturePercent below', () => {
                const user: jest.Mocked<IUser> = mocked({
                    getLedgerBalance: jest.fn().mockReturnValue(100000),
                    minEquityCapturePercent: expectedCashOnCashPercent * 300,
                    minMonthlyCashOnCashAmountHouse: 0,
                    minMonthlyCashOnCashAmountApartment: expectedCashOnCashPercent / 2,
                    rentals: null,
                    monthlySavedAmount: 0,
                    monthlyIncomeAmountGoal: 0,
                    minInvestAmount: 0,
                    maxInvestAmount: 0,
                    ledger: null,
                    metMonthlyGoal: jest.fn(),
                    hasMoneyToInvest: jest.fn().mockReturnValue(true),
                    clone: jest.fn(),
                    getAllowedInvestmentLedgerBalance: jest.fn()
                });

                expect(rental.canInvestByUser(user)).toEqual(expect.objectContaining({
                    canInvest: false,
                    investmentReason: InvestmentReasons.DoesNotMeetUserRuleEquityCapture
                }));
                expect(user.hasMoneyToInvest).toBeCalled();
                expect(user.getLedgerBalance).toBeCalled();
            });

            test('and getLedgerTotal above', () => {
                const user: jest.Mocked<IUser> = mocked({
                    getLedgerBalance: jest.fn().mockReturnValue(100000),
                    minEquityCapturePercent: expectedPurchasePrice / expectedSellPrice * 100 / 2,
                    minMonthlyCashOnCashAmountHouse: 0,
                    minMonthlyCashOnCashAmountApartment: expectedCashOnCashPercent / 2,
                    rentals: null,
                    monthlySavedAmount: 0,
                    monthlyIncomeAmountGoal: 0,
                    minInvestAmount: 0,
                    maxInvestAmount: 0,
                    ledger: null,
                    metMonthlyGoal: jest.fn(),
                    hasMoneyToInvest: jest.fn().mockReturnValue(true),
                    clone: jest.fn(),
                    getAllowedInvestmentLedgerBalance: jest.fn()
                });

                expect(rental.canInvestByUser(user)).toEqual(new UserInvestResult(true, InvestmentReasons.CanInvest));
                expect(user.hasMoneyToInvest).toBeCalled();
                expect(user.getLedgerBalance).toBeCalled();
            });
        });

        describe('and user hasMoneyToInvest false', () => {
            test('and above', () => {
                const user = new User();
                // @ts-ignore
                user.hasMoneyToInvest.mockReturnValue(false);
                // @ts-ignore
                user.getLedgerBalance.mockReturnValue(0);
                expect(rental.canInvestByUser(user)).toEqual(new UserInvestResult(false, InvestmentReasons.UserHasNoMoneyToInvest));
                expect(user.hasMoneyToInvest).toBeCalled();
                expect(user.getLedgerBalance).not.toBeCalled();
            });
        });
    });

    describe('and getRentalType', () => {
        test('should be matching', () => {
            expect(rental.getRentalType()).toEqual(RentalTypes.PassiveApartment);
        });
    });
});
