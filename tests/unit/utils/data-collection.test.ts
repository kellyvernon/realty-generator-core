'use strict';

jest.mock('../../../src/user');
jest.mock('../../../src/rentals/passive-apartment');

import {mocked} from '../../helpers/mocker';
import {evalCashSort, getRentalsBySort, getRentalsUserCanInvest} from '../../../src/utils';
import {User} from '../../../src/user';
import {IPassiveApartment, UserInvestResult} from '../../../src/rentals';

const getPassiveMock = (expectedSellEquity: number,
                 minSaleYears: number,
                 expectedRefinanceEquity: number,
                 minRefinanceMonths: number): jest.Mocked<IPassiveApartment> => {
    const rental: IPassiveApartment = {
        reserveRequirementMonthly: 0,
        cashOnCashPercent: 0,
        equityCapturePercent: 0,
        expectedCashOnCash: 0,
        expectedSaleEquity: 0,
        id: '',
        investmentAmount: 0,
        investmentPercent: 0,
        minRefinanceDate: undefined,
        minSellDate: undefined,
        purchaseDate: undefined,
        purchasePrice: 0,
        saleDate: undefined,
        sellPrice: 0,
        canInvestByUser: jest.fn(),
        canRefinance: jest.fn(),
        canSell: jest.fn(),
        cashOnCashAmountAnnual: jest.fn(),
        cashOnCashAmountMonthly: jest.fn(),
        getInvestmentPercent: jest.fn(),
        getRefinanceAmountByDate: jest.fn(),
        getSellEquityByDate: jest.fn(),
        minInvestmentAmountByUser: jest.fn(),
        saleEquityByDate: jest.fn(),
        getExpectedCashOnCashAmountAnnual: jest.fn(),
        expectedSellEquity,
        minSaleYears,
        expectedRefinanceEquity,
        minRefinanceMonths,
        refinanceDate: null,
        getRentalType: jest.fn(),
        clone: jest.fn()
    }

    return mocked(rental);
};

describe('data-collection tests', () => {
    describe('and getRentalsUserCanInvest', () => {
        test('should be same', () => {
            const i = getPassiveMock(0, 0, 0, 0);
            i.canInvestByUser.mockReturnValue(new UserInvestResult(true));
            const u = new User();

            expect(getRentalsUserCanInvest([i], u)).toEqual([i]);
        });

        test('should be empty', () => {
            const i = getPassiveMock(0, 0, 0, 0);
            i.canInvestByUser.mockReturnValue(new UserInvestResult(false));
            const u = new User();

            expect(getRentalsUserCanInvest([i], u)).toEqual([]);
        });
    });

    describe('and getRentalsBySort', () => {
        describe('and 1 rental', () => {
            test('should be true', () => {
                const i = getPassiveMock(0, 0, 0, 0);
                i.canInvestByUser.mockReturnValue(new UserInvestResult(true));
                const u = new User();

                expect(getRentalsBySort([i], u)).toEqual([i]);
            });
        });

        describe('and multiple rentals', () => {
            describe('and based on cash gained', () => {
                test('order should match', () => {
                    const lastRental = getPassiveMock(8, 8, 3, 3)
                    lastRental.getExpectedCashOnCashAmountAnnual.mockReturnValue(20);
                    const firstRental = getPassiveMock(8, 8, 3, 3)
                    firstRental.getExpectedCashOnCashAmountAnnual.mockReturnValue(20);

                    const u = new User();

                    const actual = getRentalsBySort([lastRental, firstRental], u);
                    expect(JSON.stringify(actual)).toEqual(JSON.stringify([firstRental, lastRental]));
                });

                describe('and based on cash gained', () => {
                    test('order should match', () => {
                        const lastRental = getPassiveMock(8, 8, 3, 3);
                        lastRental.getExpectedCashOnCashAmountAnnual.mockReturnValue(1);

                        const middleRental = getPassiveMock(8, 8, 3, 3);
                        middleRental.getExpectedCashOnCashAmountAnnual.mockReturnValue(10);

                        const firstRental = getPassiveMock(8, 8, 3, 3);
                        firstRental.getExpectedCashOnCashAmountAnnual.mockReturnValue(20);

                        const u = new User();

                        const actual = getRentalsBySort([lastRental, middleRental, firstRental], u);
                        expect(actual).toEqual([firstRental, middleRental, lastRental]);
                    });
                });
            });

            describe('and min investment is tie breaker', () => {
                test('order should match', () => {
                    const lastRental = getPassiveMock(8, 8, 3, 3);
                    lastRental.getExpectedCashOnCashAmountAnnual.mockReturnValue(20);

                    const firstRental = getPassiveMock(8, 8, 3, 3);
                    firstRental.getExpectedCashOnCashAmountAnnual.mockReturnValue(20);
                    firstRental.minInvestmentAmountByUser.mockReturnValue(1);

                    const u = new User();

                    const actual = getRentalsBySort([lastRental, firstRental], u);
                    expect(JSON.stringify(actual)).toEqual(JSON.stringify([firstRental, lastRental]));
                });

                test('order should match', () => {
                    const lastRental = getPassiveMock(8, 8, 3, 3);
                    lastRental.getExpectedCashOnCashAmountAnnual.mockReturnValue(20);
                    lastRental.minInvestmentAmountByUser.mockReturnValue(10);

                    const firstRental = getPassiveMock(8, 8, 3, 3);
                    firstRental.getExpectedCashOnCashAmountAnnual.mockReturnValue(20);
                    firstRental.minInvestmentAmountByUser.mockReturnValue(100);


                    const u = new User();

                    const actual = getRentalsBySort([lastRental, firstRental], u);
                    expect(actual).toEqual([lastRental, firstRental]);
                });

                test('all are same', () => {
                    const lastRental = getPassiveMock(8, 8, 3, 3);
                    lastRental.getExpectedCashOnCashAmountAnnual.mockReturnValue(20);
                    lastRental.minInvestmentAmountByUser.mockReturnValue(10);

                    const firstRental = getPassiveMock(8, 8, 3, 3);
                    firstRental.getExpectedCashOnCashAmountAnnual.mockReturnValue(20);
                    firstRental.minInvestmentAmountByUser.mockReturnValue(10);

                    const u = new User();

                    const expectedResult = [lastRental, firstRental];
                    const actual = getRentalsBySort(expectedResult, u);
                    expect(actual).toEqual(expectedResult);
                });

                test('order should match again', () => {
                    const lastRental = getPassiveMock(8, 8, 3, 3);
                    lastRental.getExpectedCashOnCashAmountAnnual.mockReturnValue(20);
                    lastRental.minInvestmentAmountByUser.mockReturnValue(20);
                    const middleRental = getPassiveMock(8, 8, 3, 3);
                    middleRental.getExpectedCashOnCashAmountAnnual.mockReturnValue(20);
                    middleRental.minInvestmentAmountByUser.mockReturnValue(10);

                    const firstRental = getPassiveMock(8, 8, 3, 3);
                    firstRental.getExpectedCashOnCashAmountAnnual.mockReturnValue(20);
                    firstRental.minInvestmentAmountByUser.mockReturnValue(1);


                    const u = new User();

                    const actual = getRentalsBySort([lastRental, middleRental, firstRental], u);
                    expect(actual).toEqual([firstRental, middleRental, lastRental]);
                });
            });
        });
    });

    describe('and evalCashSort', () => {
        test('should combine all', () => {
            const expectedSellEquity = 100;
            const minSaleYears = 5;
            const mock = getPassiveMock(expectedSellEquity, minSaleYears, 0, 0);
            const cashOnCashAmount = 10;
            mock.getExpectedCashOnCashAmountAnnual.mockReturnValue(cashOnCashAmount);
            const user = new User();

            expect(evalCashSort(mock, user)).toEqual(expectedSellEquity / minSaleYears + cashOnCashAmount);
        });

        test('should combine all including refi', () => {
            const expectedSellEquity = 100;
            const minSaleYears = 5;
            const expectedRefinanceEquity = 10;
            const minRefinanceMonths = 36;
            const mock = getPassiveMock(expectedSellEquity, minSaleYears, expectedRefinanceEquity, minRefinanceMonths * 12);
            const cashOnCashAmount = 10;
            mock.getExpectedCashOnCashAmountAnnual.mockReturnValue(cashOnCashAmount);
            const user = new User();

            expect(evalCashSort(mock, user)).toEqual(cashOnCashAmount + (expectedSellEquity / minSaleYears) + (expectedRefinanceEquity / minRefinanceMonths));
        });
    });
});
