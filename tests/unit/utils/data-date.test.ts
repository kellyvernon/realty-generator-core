'use strict';

import {isActiveDate} from '../../../src/utils';

describe('data-date tests tests', () => {
  describe('and all null', () => {
    test('should be false', () => {
      expect(isActiveDate(null, null, null)).toEqual(false);
    });
  });

  describe('and purchaseDate', () => {
    describe('and null', () => {
      test('should be false', () => {
        expect(isActiveDate(null, null, new Date(Date.now()))).toEqual(false);
      });
    });

    describe('and populated', () => {
      describe('and today prior', () => {
        test('should be false', () => {
          const purchase = new Date(Date.now());
          const today = new Date(purchase.getTime());
          today.setMonth(today.getMonth() - 1);

          expect(isActiveDate(today, purchase, null)).toEqual(false);
        });
      });

      describe('and today after', () => {
        describe('and sale null', () => {
          test('should be true', () => {
            const purchase = new Date(Date.now());
            const today = new Date(purchase.getTime());
            today.setMonth(today.getMonth() + 1);

            expect(isActiveDate(today, purchase, null)).toEqual(true);
          });
        });

        describe('and sale', () => {
          describe('and prior to today', () => {
            describe('and prior to purchase', () => {
              test('should be false', () => {
                const purchase = new Date(Date.now());

                const sale = new Date(purchase.getTime());
                sale.setMonth(purchase.getMonth() - 1);

                const today = new Date(purchase.getTime());
                today.setMonth(purchase.getMonth() + 2);

                expect(isActiveDate(today, purchase, sale)).toEqual(true);
              });
            });

            describe('and after to purchase', () => {
              test('should be true', () => {
                const purchase = new Date(Date.now());

                const today = new Date(purchase.getTime());
                today.setMonth(purchase.getMonth() + 2);

                const sale = new Date(purchase.getTime());
                sale.setMonth(purchase.getMonth() + 1);

                expect(isActiveDate(today, purchase, sale)).toEqual(true);
              });
            });

            describe('and after to today', () => {
              describe('and after to purchase', () => {
                test('should be false', () => {
                  const purchase = new Date(Date.now());

                  const today = new Date(purchase.getTime());
                  today.setMonth(today.getMonth() + 2);

                  const sale = new Date(today.getTime());
                  sale.setMonth(today.getMonth() + 1);

                  expect(isActiveDate(today, purchase, sale)).toEqual(false);
                });
              });
            });
          });
        });
      });
    });
  });
});
