'use strict';

import {ILedgerItem, LedgerItem, Ledger, LedgerItemType} from '../../src/ledger';

describe('ledger tests', () => {
    let expected: ILedgerItem;

    let createdDate: Date;
    let ledger: Ledger;

    beforeEach(() => {
        createdDate = new Date(Date.now());

        const expectedAmount = 10;
        expected = {
            created: createdDate,
            type: LedgerItemType.CashFlow,
            amount: expectedAmount,
            clone: jest.fn()
        };

        ledger = new Ledger();
        ledger.pushLedgerItem({
            created: createdDate,
            type: LedgerItemType.CashFlow,
            amount: expectedAmount,
            clone: jest.fn()
        });
    });

    afterEach(() => {
        expected = null;
        ledger = null;
    });

    describe('and balance', () => {
        describe('1 item', () => {
            test('balance should be amount', () => {
                expect(expected.amount).toEqual(ledger[0].balance);
            });

            test('and first', () => {
                expect(JSON.stringify(ledger.first())).toEqual(JSON.stringify({
                    item: expected,
                    balance: expected.amount
                }));
            });
        });

        test('should roll balance', () => {
            const ledgerItem: ILedgerItem = {
                created: expected.created,
                type: expected.type,
                amount: expected.amount,
                clone: jest.fn()
            }

            ledger.pushLedgerItem(ledgerItem);

            expect(JSON.stringify(ledger)).toEqual(JSON.stringify([{
                item: expected,
                balance: expected.amount
            }, {
                item: ledgerItem,
                balance: expected.amount + expected.amount
            }]));

            expect(ledger.last()).toEqual({item: ledgerItem, balance: expected.amount + expected.amount});
        });

        test('should roll balance', () => {
            const secondAmount = expected.amount * -7.5;
            const secondAddedLedgerItem: ILedgerItem = {
                created: expected.created,
                type: LedgerItemType.Other,
                amount: secondAmount,
                clone: jest.fn()
            }

            ledger.pushLedgerItem(secondAddedLedgerItem);

            expect(JSON.stringify(ledger)).toEqual(JSON.stringify([{
                item: expected,
                balance: expected.amount
            }, {
                item: secondAddedLedgerItem,
                balance: expected.amount + secondAmount
            }]));
        });
    });

    describe('and getMonthlyBalance', () => {
        test('should get MonthlyBalance', () => {
            const ledgerItem1 = new LedgerItem(null);
            ledgerItem1.amount = expected.amount;
            ledgerItem1.created = expected.created;
            ledgerItem1.type = LedgerItemType.Other;

            ledger.pushLedgerItem(ledgerItem1);

            expect(ledger.getMonthlySummary(expected.created).balance).toEqual(expected.amount * 2);
        });

        test('should get total for month', () => {
            const ledgerItem = new LedgerItem(null);
            ledgerItem.amount = expected.amount;
            ledgerItem.created = new Date(expected.created.getFullYear(), expected.created.getMonth() + 3, expected.created.getDate());

            ledger.pushLedgerItem(ledgerItem);

            expect(ledger.getMonthlySummary(expected.created).balance).toEqual(expected.amount);
        });
    });

    test('should get total for year', () => {
        const ledgerItem = new LedgerItem(null);
        ledgerItem.amount = expected.amount;
        ledgerItem.note = null;
        ledgerItem.created = new Date(expected.created.getFullYear() + 2, expected.created.getMonth() + 3, expected.created.getDate());
        ledgerItem.type = LedgerItemType.Other;

        ledger.pushLedgerItem(ledgerItem);

        expect(ledger.getAnnualSummary(expected.created).balance).toEqual(expected.amount);
    });

    describe('and total', () => {
        test('should get total', () => {
            const ledgerItem = new LedgerItem(null);
            ledgerItem.amount = expected.amount;
            ledgerItem.created = new Date(expected.created);
            ledgerItem.type = LedgerItemType.Other;

            ledger.pushLedgerItem(ledgerItem);

            const ledgerItem1 = new LedgerItem(null);
            ledgerItem1.amount = expected.amount * -1;
            ledgerItem1.created = new Date(expected.created.getFullYear() + 2, expected.created.getMonth() + 3, expected.created.getDate());
            ledgerItem1.type = LedgerItemType.Other;

            ledger.pushLedgerItem(ledgerItem1);

            const ledgerItem2 = new LedgerItem(null);
            ledgerItem2.amount = expected.amount * -1;
            ledgerItem2.created = new Date(expected.created.getFullYear() + 2, expected.created.getMonth() + 3, expected.created.getDate());
            ledgerItem2.type = LedgerItemType.Other;

            ledger.pushLedgerItem(ledgerItem1);
            expect(ledger.total()).toEqual(0);
        });
        test('and no collection', () => {
            const l = new Ledger();
            expect(l.total()).toEqual(0);
        });
    });

    describe('and isPopulated', () => {
        it('is empty', () => {
            const l = new Ledger();
            expect(l.isPopulated()).toBeFalsy();
        });

        it('and contains item', () => {
            expect(ledger.isPopulated()).toBeTruthy();
        });

        it('and collection is null', () => {
            const l = new Ledger();
            expect(l.isPopulated()).toBeFalsy();
        });
    });

    describe('and totalYears', () => {
        it('is empty', () => {
            const l = new Ledger();
            expect(l.totalYears()).toBe(-1);
        });

        it('and contains item', () => {
            const l = new Ledger();
            const date = new Date(Date.now());
            const date2 = new Date(Date.now());

            const li1 = new LedgerItem(null);
            li1.created = date;


            const numberOfYears = 3;
            date2.setFullYear(date2.getFullYear() + numberOfYears);

            const li2 = new LedgerItem(null);
            li2.created = date2;


            l.pushLedgerItem(li1);
            l.pushLedgerItem(li2);

            expect(l.totalYears()).toEqual(numberOfYears);
        });
    });

    describe('and totalMonths', () => {
        it('is empty', () => {
            const l = new Ledger();
            expect(l.totalMonths()).toBe(-1);
        });

        it('and contains item', () => {
            const l = new Ledger();
            const date = new Date(Date.now());
            const date2 = new Date(Date.now());
            const numberOfYears = 3;

            const li1 = new LedgerItem(null);
            li1.created = date;

            l.pushLedgerItem(li1);

            date2.setFullYear(date2.getFullYear() + numberOfYears);

            const li2 = new LedgerItem(null);
            li2.created = date2;


            l.pushLedgerItem(li2);

            expect(l.totalMonths()).toEqual(numberOfYears * 12);
        });
    });

    describe('and last', () => {
        it('is empty', () => {
            const l = new Ledger();
            expect(l.last()).toEqual({balance: 0, item: null});
        });
    });

    describe('and first', () => {
        it('is empty', () => {
            const l = new Ledger();
            expect(l.first()).toEqual({balance: 0, item: null});
        });
    });
});
