'use strict';

import * as momentRange from 'moment-range';
import * as moment from 'moment';
const extendMoment = momentRange.extendMoment(moment);

export function isValidCashFlowTime(purchased: Date, sold: Date, today: Date) {
    if (!purchased || purchased.getTime() > today.getTime()) {
        return false;
    }

    if (!sold) {
        return true;
    }

    return !(sold.getTime() < today.getTime());
}

export function isActiveDate(today: Date, purchase: Date, sold?: Date): boolean {
    if (!purchase || !today) {
        return false;
    }

    const compareTodayToPurchase = today.getTime() - purchase.getTime();
    if (compareTodayToPurchase < 0) {
        return false;
    }

    if (compareTodayToPurchase >= 0) {
        if (!sold) {
            return true;
        }

        return sold.getTime() <= today.getTime();
    }

    return false;
}

export function totalMonths(purchased: Date, today: Date) {
    const range = extendMoment.range(purchased, today);

    return Array.from(range.by('month')).length;
}
