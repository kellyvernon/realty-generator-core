'use strict';

import {IUser} from '../user';
import {IRental, UserInvestResult} from '..';

export function evalCashSort(rental: IRental, usr: IUser): number {
    let total = rental.getExpectedCashOnCashAmountAnnual(rental.minInvestmentAmountByUser(usr));
    total += rental.expectedSellEquity / rental.minSaleYears;
    if (rental.minRefinanceMonths > 0) {
        total += rental.expectedRefinanceEquity / (rental.minRefinanceMonths / 12);
    }
    return total;
}

export function getRentalsUserCanInvest(rentals: IRental[], user: IUser): IRental[] {
    return rentals.filter(r => r.canInvestByUser(user).canInvest);
}

export function getRentalsUserCanNotInvest(rentals: IRental[], user: IUser): { id: string, result: UserInvestResult }[] {
    return rentals.filter(r => !r.canInvestByUser(user).canInvest).map(r => ({
        id: r.id,
        result: r.canInvestByUser(user)
    }));
}

export function getRentalsBySort(rentals: IRental[], user: IUser): IRental[] {
    return rentals.sort((r1: IRental, r2: IRental): number => {
        const r1Cash: number = evalCashSort(r1, user);
        const r2Cash: number = evalCashSort(r2, user);
        if (r1Cash > r2Cash) {
            return -1;
        }

        if (r1Cash < r2Cash) {
            return 1;
        }

        if (r1.minInvestmentAmountByUser(user) > r2.minInvestmentAmountByUser(user)) {
            return 1;
        }

        if (r1.minInvestmentAmountByUser(user) < r2.minInvestmentAmountByUser(user)) {
            return -1;
        }

        return 0;
    });
}
