'use strict';

import {Rental} from '../rentals';
import {fixedAndFloat, randomNumberBetween} from '../utils';
import {IValueCache} from '../value-cache';
import {IRentalGeneratorOptions} from './i-rental-generator-options';
import {ICreateOptions} from './i-create-options';

export type OptionsTranslator = (createOptions: ICreateOptions) => any;

const create = <T extends Rental>(createOptions: ICreateOptions, rentalClass: new(...args: any[]) => T, optionTranslator: OptionsTranslator): T => {
    if (typeof optionTranslator === 'function') {
        const source: any = optionTranslator(createOptions);
        createOptions = Object.assign({}, source);
    }

    return new rentalClass(createOptions) as T;
};

export interface IRentalGenerator extends IRentalGeneratorOptions {
    rentalType: typeof Rental;

    getRentals(today?: Date): Rental[];

    removeRentalById(id: string, today?: Date): void;
}

export class RentalGenerator implements IRentalGenerator {
    public renewalInMonths: number;
    public minRentalOpportunities: number;
    public maxRentalOpportunities: number;

    public lowestPriceDown: number;
    public highestPriceDown: number;

    public lowestSellPercent: number;
    public highestSellPercent: number;

    public lowestMinSellInYears: number;
    public highestMinSellInYears: number;

    public lowestCashOnCashPercent: number;
    public highestCashOnCashPercent: number;

    public lowestMinRefinanceInMonths: number;
    public highestMinRefinanceInMonths: number;

    public lowestRefinancePercent: number;
    public highestRefinancePercent: number;

    public rentalType: typeof Rental;
    public translate?: OptionsTranslator;

    constructor(cache: IValueCache) {
        this._rentalCache = cache;
    }

    public getRentals(today?: Date): Rental[] {
        if (this.maxRentalOpportunities === 0) {
            this._rentalCache.setValue([], today);
            return this._rentalCache.getValue(today);
        }

        if (this._rentalCache.getValue(today).length > 0) {
            return this._rentalCache.getValue(today);
        }

        const totalRandom = this.maxRentalOpportunities === 1
            ? 1 :
            randomNumberBetween(this.maxRentalOpportunities <= 1 ? 1 : this.maxRentalOpportunities, this.maxRentalOpportunities + 1);

        const rentalType = this.rentalType;

        for (let i = 0; i < totalRandom; i++) {
            const purchasePrice = fixedAndFloat(randomNumberBetween(this.lowestPriceDown, this.highestPriceDown));
            const createOptions: ICreateOptions = {
                purchaseDate: null,
                saleDate: null,
                minSaleYears: randomNumberBetween(this.lowestMinSellInYears, this.highestMinSellInYears),

                purchasePrice,
                sellPrice: fixedAndFloat(randomNumberBetween(this.lowestSellPercent, this.highestSellPercent) / 100 * purchasePrice, 0),

                refinancePercent: randomNumberBetween(this.lowestRefinancePercent, this.highestRefinancePercent),
                minRefinanceMonths: randomNumberBetween(this.lowestMinRefinanceInMonths, this.highestMinRefinanceInMonths)
            };

            this._rentalCache.getValue(today).push(create(createOptions, rentalType, this.translate));
        }

        return this._rentalCache.getValue(today);
    }

    public removeRentalById(id: string, today?: Date): void {
        const rentals = this.getRentals(today);
        if (rentals.length === 0) {
            return;
        }

        this._rentalCache.setValue(rentals.filter(i => i.id !== id), today);
    }

    private _rentalCache: IValueCache;
}
