import {IRentalGenerator} from './rental-generator';
import {RentalTypes} from '../rentals';

export interface IGeneratorTyping {
    rentalType: RentalTypes;
    generator: IRentalGenerator;
}

