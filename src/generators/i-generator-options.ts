import {RentalTypes} from '../rentals';
import {IRentalGeneratorOptions} from './i-rental-generator-options';

export interface IGeneratorOptions {
    rentalType: RentalTypes;
    options: IRentalGeneratorOptions;
}
