import {OptionsTranslator} from './rental-generator';

export interface IRentalGeneratorOptions {
    highestMinRefinanceInMonths: number;
    lowestMinRefinanceInMonths: number;

    highestRefinancePercent: number;
    lowestRefinancePercent: number;

    renewalInMonths: number;

    minRentalOpportunities: number;
    maxRentalOpportunities: number;

    lowestPriceDown: number;
    highestPriceDown: number;

    lowestSellPercent: number;
    highestSellPercent: number;

    lowestMinSellInYears: number;
    highestMinSellInYears: number;

    lowestCashOnCashPercent: number;
    highestCashOnCashPercent: number;

    translate?: OptionsTranslator
}
