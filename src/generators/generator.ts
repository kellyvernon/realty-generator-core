'use strict';

import * as moment0 from 'moment';

import * as momentRange from 'moment-range';
import {LedgerItem, LedgerItemType} from '../ledger';
import {Rental, PassiveApartment, RentalTypes, SingleFamily} from '../rentals';
import {IUser} from '../user';

import {getRentalsBySort, getRentalsUserCanInvest, getRentalsUserCanNotInvest} from '../utils';
import {GeneratorTimeLine, GeneratorTimeLineRental, IRentalReasonTimeLine} from '../timeline';
import {IGeneratorOptions} from './i-generator-options';
import {IGeneratorTyping} from './i-generator-typing';

const moment = momentRange.extendMoment(moment0);

export const getClassType = (rentalType: RentalTypes): typeof Rental => {
    if (rentalType === RentalTypes.PassiveApartment) {
        return PassiveApartment;
    }

    return SingleFamily;
}

export interface IRunEntryResult {
    user: IUser;
    rentals?: GeneratorTimeLineRental[];
    today: Date;
    rejectReasons?: IRentalReasonTimeLine[];
}

interface IGenerator {
    simulate(user: IUser, rentalOptions: IGeneratorOptions[], maxYears: number): Promise<GeneratorTimeLine>;
}

export class Generator implements IGenerator {

    constructor(rentalCacheCollection: IGeneratorTyping[]) {
        this._rentalCacheCollection = rentalCacheCollection || [];
    }

    public async simulate(user: IUser, rentalOptions: IGeneratorOptions[] = [], maxYears: number = 10): Promise<GeneratorTimeLine> {
        let start: Date = new Date(Date.now());

        if (user.rentals.isPopulated()) {
            start = new Date(user.rentals.first().purchaseDate.getFullYear(), user.rentals.first().purchaseDate.getMonth(), 1);
        }

        let today: Date = new Date(start.getUTCFullYear(), start.getUTCMonth(), 1);

        this.setupCache(rentalOptions);

        if (!user.hasMoneyToInvest()) {
            const cannotInvestTimeLine = new GeneratorTimeLine();
            cannotInvestTimeLine.user = user;
            cannotInvestTimeLine.rentals = [];
            cannotInvestTimeLine.rejectReasons = [];
            cannotInvestTimeLine.date = today;
            return cannotInvestTimeLine;
        }

        let result: IRunEntryResult = {
            user,
            today
        };

        while (this.hasMetGoalOrMaxTime(start, today, user, maxYears) === false) {
            result = this.runEntry({
                user: result.user.clone(),
                rentals: result.rentals,
                rejectReasons: result.rejectReasons,
                today: new Date(today.getUTCFullYear(), today.getUTCMonth(), today.getDate())
            });

            today = new Date(today.getUTCFullYear(), today.getUTCMonth() + 1, 1);
        }

        const resultTimeLine = new GeneratorTimeLine();
        resultTimeLine.rentals = result.rentals;
        resultTimeLine.rejectReasons = result.rejectReasons;
        resultTimeLine.user = result.user;
        resultTimeLine.date = today;
        return resultTimeLine;
    }

    public hasMetGoalOrMaxTime(start: Date, current: Date, user: IUser, maxYears: number): boolean {
        const moments = Array.from(moment.range(start, current).by('year')); /*?*/
        if (moments.length >= maxYears + 1) {
            return true;
        }

        return user.metMonthlyGoal(current);
    }

    public runEntry(runEntryResult: IRunEntryResult): IRunEntryResult {
        const currentRun: IRunEntryResult = {
            user: runEntryResult.user,
            rentals: runEntryResult.rentals || [],
            rejectReasons: runEntryResult.rejectReasons || [],
            today: runEntryResult.today
        };

        if (currentRun.user.monthlySavedAmount > 0) {
            currentRun.user.ledger.pushLedgerItem(new LedgerItem({
                amount: currentRun.user.monthlySavedAmount,
                type: LedgerItemType.Income,
                created: new Date(currentRun.today.getTime()),
                note: 'saved for month'
            }));
        }

        currentRun.user.rentals.currentCashFlowByRental(currentRun.today).forEach(rental => {
            currentRun.user.ledger.pushLedgerItem(new LedgerItem({
                amount: rental.cashOnCashAmountMonthly(currentRun.today),
                type: LedgerItemType.CashFlow,
                created: new Date(currentRun.today.getTime()),
                note: 'cashflow',
                referenceId: rental.id
            }));
        });

        currentRun.user.rentals.rentalsForRefinance(currentRun.today).forEach(rentalIndex => {
            currentRun.user.ledger.pushLedgerItem(new LedgerItem({
                amount: currentRun.user.rentals.get(rentalIndex).getRefinanceAmountByDate(currentRun.today),
                type: LedgerItemType.Equity,
                created: new Date(currentRun.today.getTime()),
                note: 'refinance',
                referenceId: currentRun.user.rentals.get(rentalIndex).id
            }));

            currentRun.user.rentals.get(rentalIndex).refinanceDate = new Date(currentRun.today.getTime());
        });

        currentRun.user.rentals.rentalsForSale(currentRun.today).forEach(rentalsForSale => {
            rentalsForSale.saleDate = new Date(currentRun.today.getTime());
            currentRun.user.ledger.pushLedgerItem(new LedgerItem({
                amount: rentalsForSale.saleEquityByDate(currentRun.today),
                type: LedgerItemType.Equity,
                created: new Date(currentRun.today.getTime()),
                note: 'sold',
                referenceId: rentalsForSale.id
            }));
        });

        let allGeneratedRentals: Rental[] = [];
        this._rentalCacheCollection.forEach((value) => {
            allGeneratedRentals = allGeneratedRentals.concat(value.generator.getRentals(currentRun.today));
        });

        allGeneratedRentals.forEach(r => {
            if (!currentRun.rentals.some(p => p.rental.id === r.id)) {
                currentRun.rentals.push(new GeneratorTimeLineRental(new Date(currentRun.today.getTime()), r));
            }
        });

        // TODO: need to compare sf and mf, to see what the better deal is, then do buy in that order
        getRentalsBySort(
            getRentalsUserCanInvest(allGeneratedRentals, currentRun.user),
            currentRun.user).forEach((currentPossibleRental) => {

            if (currentPossibleRental.canInvestByUser(currentRun.user)) {
                currentPossibleRental.purchaseDate = new Date(currentRun.today.getTime());
                currentPossibleRental.investmentAmount = currentPossibleRental.minInvestmentAmountByUser(currentRun.user);

                currentRun.user.rentals.push(currentPossibleRental);
                currentRun.user.ledger.pushLedgerItem(new LedgerItem({
                    amount: currentPossibleRental.minInvestmentAmountByUser(currentRun.user) * -1,
                    type: LedgerItemType.Purchase,
                    created: new Date(currentRun.today.getTime()),
                    note: 'purchase',
                    referenceId: currentPossibleRental.id
                }));

                this._rentalCacheCollection.forEach(cache => {
                    if (cache.rentalType === currentPossibleRental.getRentalType()) {
                        cache.generator.removeRentalById(currentPossibleRental.id, currentRun.today);
                    }
                });

                const idx = currentRun.rentals.findIndex(r => r.rental.id === currentPossibleRental.id);
                if (idx !== -1) {
                    currentRun.rentals[idx].rental.purchaseDate = new Date(currentRun.today.getTime());
                    currentRun.rentals[idx].rental.investmentAmount = currentPossibleRental.minInvestmentAmountByUser(currentRun.user);
                }
            }
        });

        currentRun.rejectReasons = currentRun.rejectReasons.concat(getRentalsUserCanNotInvest(allGeneratedRentals, currentRun.user).map(reason => ({
            id: reason.id,
            result: reason.result,
            date: currentRun.today
        })));

        return currentRun;
    }

    private _rentalCacheCollection: IGeneratorTyping[];

    private setupCache(rentalOptions: IGeneratorOptions[] = []): void {
        this._rentalCacheCollection.forEach((value: IGeneratorTyping) => {
            const found: IGeneratorOptions = rentalOptions.find(o => o.rentalType === value.rentalType);
            if (found) {
                value.generator.highestMinRefinanceInMonths = found.options.highestMinRefinanceInMonths;
                value.generator.lowestMinRefinanceInMonths = found.options.lowestMinRefinanceInMonths;
                value.generator.highestRefinancePercent = found.options.highestRefinancePercent;
                value.generator.lowestRefinancePercent = found.options.lowestRefinancePercent;
                value.generator.renewalInMonths = found.options.renewalInMonths;
                value.generator.minRentalOpportunities = found.options.minRentalOpportunities;
                value.generator.maxRentalOpportunities = found.options.maxRentalOpportunities;
                value.generator.lowestPriceDown = found.options.lowestPriceDown;
                value.generator.highestPriceDown = found.options.highestPriceDown;
                value.generator.lowestSellPercent = found.options.lowestSellPercent;
                value.generator.highestSellPercent = found.options.highestSellPercent;
                value.generator.lowestMinSellInYears = found.options.lowestMinSellInYears;
                value.generator.highestMinSellInYears = found.options.highestMinSellInYears;
                value.generator.lowestCashOnCashPercent = found.options.lowestCashOnCashPercent;
                value.generator.highestCashOnCashPercent = found.options.highestCashOnCashPercent;
                value.generator.rentalType = getClassType(found.rentalType);
                value.generator.translate = found.options.translate;
            }
        });
    }
}
