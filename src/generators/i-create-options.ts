export interface ICreateOptions {
    purchaseDate?: Date;
    saleDate?: Date;
    minSaleYears: number;

    /**
     * Is the amount down, not the total value of the place
     */
    purchasePrice: number;

    /**
     * is the purchase price with sale percent applied.
     */
    sellPrice: number;

    refinancePercent: number;
    minRefinanceMonths: number;
}
