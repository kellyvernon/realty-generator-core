import {UserInvestResult} from '../rentals';

export interface IRentalReasonTimeLine {
    date: Date;
    result: UserInvestResult;
    id: string;
}

