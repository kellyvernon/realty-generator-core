'use strict';

import {IUser} from '../user';
import {GeneratorTimeLineRental} from './generator-time-line-rental';
import {IRentalReasonTimeLine} from './i-rental-reason-time-line';

export class GeneratorTimeLine {
    public user: IUser;
    public date: Date;
    public rentals: GeneratorTimeLineRental[];
    public rejectReasons: IRentalReasonTimeLine[];
}
