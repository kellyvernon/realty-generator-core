'use strict';

import {IRental} from '../rentals';

export class GeneratorTimeLineRental {
    public date: Date;
    public rental: IRental;

    constructor(date, rental: IRental) {
        this.date = date;
        this.rental = rental;
    }
}
