'use strict';

import {LedgerItemType} from './ledger-item-type';

export interface ILedgerOptions {
    amount: number;
    type: LedgerItemType;
    note?: string;
    created?: Date;
    referenceId?: string;
}

export interface ILedgerItem extends ILedgerOptions {
    clone(): ILedgerItem;
}

export class LedgerItem implements ILedgerOptions, ILedgerItem {
    public amount: number;
    public type: LedgerItemType;
    public created?: Date;
    public note?: string;
    public referenceId?: string;

    constructor(options?: ILedgerOptions) {
        const fullDate = new Date(Date.now());
        const optionDefaults: ILedgerOptions = {
            amount: 0,
            type: LedgerItemType.Other,
            note: '',
            created: new Date(fullDate.getUTCFullYear(), fullDate.getUTCMonth(), 1),
            referenceId: null
        };

        options = options ? options : optionDefaults;

        this.amount = parseFloat(options.amount.toFixed(2));
        this.type = options.type || optionDefaults.type;
        this.created = options.created;
        this.note = options.note || optionDefaults.note;
        this.referenceId = options.referenceId || options.referenceId;
    }

    public clone(): ILedgerItem {
        return new LedgerItem({
            amount: this.amount,
            type: this.type,
            referenceId: this.referenceId,
            note: this.note,
            created: this.created
        });
    }
}
