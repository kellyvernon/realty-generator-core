'use strict';

export enum LedgerItemType {
    CashFlow = 'CashFlow',
    Income = 'Income',
    Purchase = 'Purchase',
    Equity = 'Equity Capture',
    Other = 'Other',
}

