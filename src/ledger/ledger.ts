'use strict';

import * as moment from 'moment';
import * as momentRange from 'moment-range';
import {ILedgerItem} from './ledger-item';
import {LedgerItemType} from './ledger-item-type';
import {Collection} from '../common';

const extendMoment = momentRange.extendMoment(moment);

export interface ILedgerSummary {
    date: Date;
    balance: number;
    cashFlow: number;
    averageCashFlow: number;
    purchases: number;
    equity: number;
}

export interface ILedgerItemBalance {
    item: ILedgerItem;
    balance: number;
}

export interface ILedger {
    pushLedgerItem(entity: ILedgerItem): void;

    getSummaryByFilter(predicate: LedgerItemPredicate): ILedgerSummary;

    getMonthlySummary(date: Date): ILedgerSummary;

    getAnnualSummary(date: Date): ILedgerSummary;

    getMonthlySummaries(year: number): ILedgerSummary[];

    getAnnualSummaries(): ILedgerSummary[];

    getLedgerItemBalances(date: Date): ILedgerItemBalance[];

    total(): number;

    totalMonths(): number;

    totalYears(): number;

    last(): ILedgerItemBalance;

    first(): ILedgerItemBalance;

    isPopulated(): boolean;

    clone(): ILedger;
}

export type LedgerItemPredicate = (entity: ILedgerItemBalance) => boolean;

export class Ledger extends Collection<ILedgerItemBalance> implements ILedger {
    public pushLedgerItem(entity: ILedgerItem): void {
        this.push({
            item: entity,
            balance: this.length === 0 ? entity.amount : this.last().balance + entity.amount
        });
    }

    public getSummaryByFilter(predicate: LedgerItemPredicate): ILedgerSummary {
        const summary: ILedgerSummary = {
            date: null,
            balance: 0,
            cashFlow: 0,
            averageCashFlow: 0,
            equity: 0,
            purchases: 0,
        };

        if (this.length === 0) {
            return summary;
        }

        const itemBalances = this.filter(i => predicate(i));
        itemBalances.reduce((sum, currentLedgerBalance) => {
            sum.balance = currentLedgerBalance.balance;
            if (!sum.date) {
                sum.date = currentLedgerBalance.item.created;
            }
            return sum;
        }, summary);

        itemBalances.filter(x => x.item.type === LedgerItemType.CashFlow).reduce((sum, currentLedgerBalance) => {
            sum.cashFlow = sum.cashFlow + currentLedgerBalance.item.amount;
            return sum;
        }, summary);
        summary.averageCashFlow = summary.cashFlow === 0 ? 0 : summary.cashFlow / itemBalances.length;

        itemBalances.filter(x => x.item.type === LedgerItemType.Purchase).reduce((sum, currentLedgerBalance) => {
            sum.purchases = sum.purchases + currentLedgerBalance.item.amount;
            return sum;
        }, summary);

        itemBalances.filter(x => x.item.type === LedgerItemType.Equity).reduce((sum, currentLedgerBalance) => {
            sum.equity = sum.equity + currentLedgerBalance.item.amount;
            return sum;
        }, summary);

        return summary;
    }

    public getMonthlySummary(date: Date): ILedgerSummary {
        return this.getSummaryByFilter(ele => date.getMonth() === ele.item.created.getMonth() && date.getFullYear() === ele.item.created.getFullYear())
    }

    public getAnnualSummary(date: Date): ILedgerSummary {
        const summaryByFilter = this.getSummaryByFilter(ele => date.getFullYear() === ele.item.created.getFullYear());
        summaryByFilter.averageCashFlow = summaryByFilter.cashFlow === 0 ? 0 : summaryByFilter.cashFlow / 12;
        return summaryByFilter;
    }

    public getAnnualSummaries(): ILedgerSummary[] {
        const collection: ILedgerSummary[] = [];

        const first = this.first().item.created || new Date(Date.now());
        const last = this.last().item.created || new Date(Date.now());

        for (let i = first.getFullYear(); i < last.getFullYear() + 1; i++) {
            const curDate: Date = new Date(i, 1, 1);
            const annualBalance = this.getAnnualSummary(curDate);
            annualBalance.averageCashFlow = annualBalance.cashFlow === 0 ? 0 : annualBalance.cashFlow / 12;
            collection.push(annualBalance);
        }

        return collection;
    }

    public getMonthlySummaries(year: number): ILedgerSummary[] {
        const collection: ILedgerSummary[] = [];
        const findFirstMonthOfYear = this.find(i => i.item.created.getFullYear() === year);

        for (let month = findFirstMonthOfYear.item.created.getMonth(); month < 12; month++) {
            const curDate: Date = new Date(year, month, 1);
            const monthlyBalance = this.getMonthlySummary(curDate);
            collection.push(monthlyBalance);
        }

        return collection.filter(x => !!x.date);
    }

    public getLedgerItemBalances(date: Date): ILedgerItemBalance[] {
        if (!date) {
            return [];
        }
        return this.filter(x => x.item.created.getMonth() === date.getMonth() && x.item.created.getFullYear() === date.getFullYear());
    }

    public total(): number {
        return parseFloat(this.last().balance.toFixed(2));
    }

    public totalMonths(): number {
        if (!this.isPopulated()) {
            return -1;
        }

        const start = this.first().item.created;
        const final = this.last().item.created;

        const range = extendMoment.range(start, final);

        return Array.from(range.by('month')).length - 1;
    }

    public totalYears(): number {
        if (!this.isPopulated()) {
            return -1;
        }

        return this.totalMonths() / 12;
    }

    public last(): ILedgerItemBalance {
        if (super.last() === null) {
            return {
                balance: 0,
                item: null
            };
        }

        return super.last();
    }

    public first(): ILedgerItemBalance {
        if (super.first() === null) {
            return {
                balance: 0,
                item: null
            };
        }

        return super.first();
    }

    public clone(): ILedger {
        const ledger = new Ledger();
        this.forEach(i => {
            ledger.pushLedgerItem(i.item.clone())
        });
        return ledger;
    }
}


