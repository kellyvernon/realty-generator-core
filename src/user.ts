'use strict';

import {ILedger} from './ledger';
import {RentalCollection} from './rentals';

/**
 * the reason why there are two of minMonthlyCashOnCashAmount* is that you may want more cashflow for a house
 * than an apartment. Typically investors will get more cashflow (higher CoC) on houses.
 */
export interface IUserOptions {
    minMonthlyCashOnCashAmountHouse?: number;
    minMonthlyCashOnCashAmountApartment?: number;
    monthlyIncomeAmountGoal?: number;
    monthlySavedAmount?: number;
    minEquityCapturePercent?: number;
    minInvestAmount?: number;
    maxInvestAmount?: number;
}

/**
 * TODO: probably need a user rules engine. Right now the user has hard constraints. Here's the minimum equity, here's the min cashflow.
 * If any one of these doesn't align, then it's no good, so the idea should be flexible. Om pne example, I saw a CoC of 14%, yet it was rejected
 */

export interface IUser {
    ledger: ILedger;
    rentals: RentalCollection;
    readonly monthlyIncomeAmountGoal: number;
    readonly monthlySavedAmount: number;
    readonly minMonthlyCashOnCashAmountHouse: number;
    readonly minMonthlyCashOnCashAmountApartment: number;
    readonly minEquityCapturePercent: number;
    readonly minInvestAmount: number;
    readonly maxInvestAmount: number;

    getAllowedInvestmentLedgerBalance(): number;

    getLedgerBalance(): number;

    metMonthlyGoal(today: Date): boolean;

    hasMoneyToInvest(): boolean;

    clone(): IUser;
}

export class User implements IUser {
    get ledger() {
        return this._ledger;
    }

    set ledger(value) {
        this._ledger = value;
    }

    get rentals() {
        return this._rentals;
    }

    set rentals(value) {
        this._rentals = value;
    }

    get monthlyIncomeAmountGoal() {
        return this._options.monthlyIncomeAmountGoal;
    }

    get monthlySavedAmount() {
        return this._options.monthlySavedAmount;
    }

    get minMonthlyCashOnCashAmountHouse() {
        return this._options.minMonthlyCashOnCashAmountHouse;
    }

    get minMonthlyCashOnCashAmountApartment() {
        return this._options.minMonthlyCashOnCashAmountApartment;
    }

    get minEquityCapturePercent() {
        return this._options.minEquityCapturePercent;
    }

    get minInvestAmount() {
        return this._options.minInvestAmount;
    }

    get maxInvestAmount() {
        return this._options.maxInvestAmount;
    }

    constructor(options: IUserOptions = {}) {
        this._options = {
            monthlyIncomeAmountGoal: options.monthlyIncomeAmountGoal || 0,
            monthlySavedAmount: options.monthlySavedAmount || 0,
            minMonthlyCashOnCashAmountApartment: options.minMonthlyCashOnCashAmountApartment || 0,
            minMonthlyCashOnCashAmountHouse: options.minMonthlyCashOnCashAmountHouse || 0,
            minEquityCapturePercent: options.minEquityCapturePercent || 0,
            minInvestAmount: options.minInvestAmount || 0,
            maxInvestAmount: options.maxInvestAmount || 0,
        };

        this._ledger = null;

        this._rentals = null;
    }

    public metMonthlyGoal(today: Date): boolean {
        if (!today) {
            return false;
        }

        if (!this.rentals) {
            return false;
        }

        return this.rentals.cashOnCashAmountMonthly(today) >= this.monthlyIncomeAmountGoal;
    }

    public hasMoneyToInvest(): boolean {
        if (this.getLedgerBalance() === 0) {
            return false;
        }

        return this.getAllowedInvestmentLedgerBalance() > 0;
    }

    public getLedgerBalance(): number {
        if (!this.ledger) {
            return 0;
        }

        return this.ledger.total();
    }

    public getAllowedInvestmentLedgerBalance(): number {
        const reserveRequirementMonthly = !this.rentals ? 0 : this.rentals.reserveRequirementMonthly();
        return this.getLedgerBalance() - reserveRequirementMonthly;
    }

    public clone(): IUser {
        const user: IUser = new User({...this._options});
        user.rentals = this.rentals.clone();
        user.ledger = this.ledger.clone();
        return user;
    }

    private readonly _options: IUserOptions;
    private _ledger: ILedger;
    private _rentals: RentalCollection = null;

    /*
     *  TODO: I might need to come up with rental rules
     *  For example, one for Single Family would be amount needed for savings
     *  per house, and when multiples ... I think it's 1, 4 per, then after 5 combined (reduced amount
     *  how many houses to hold before selling
     */

}
