'use strict';

import {IUser} from '../user';

import {currencyFormatter, fixedAndFloat, isValidCashFlowTime, totalMonths} from '../utils';
import {IRental, IRentalOptions, Rental} from './rental';
import {InvestmentReasons, UserInvestResult} from './investment-reasons';

export interface IPassiveApartmentOptions extends IRentalOptions {
    investmentAmounts?: number[];
    cashOnCashPercent?: number;
}

export interface IPassiveApartment extends IRental {
    readonly expectedSaleEquity: number;

    saleEquityByDate(today: Date): number;
}

export class PassiveApartment extends Rental implements IPassiveApartment {

    get cashOnCashPercent() {
        return this._options.cashOnCashPercent;
    }

    get expectedSaleEquity(): number {
        return fixedAndFloat(this.equityCapturePercent * this.investmentAmount / 100);
    }

    constructor(options: IPassiveApartmentOptions = {}) {
        super(options);
        this.id = `PA${this.id}`;
        const optionDefaults: IPassiveApartmentOptions = {
            investmentAmounts: [0],
            refinancePercent: 0,
            cashOnCashPercent: 0,
            minRefinanceMonths: 0,
            purchaseDate: null,
            minSaleYears: 0,
            saleDate: null,
            sellPrice: 0,
            purchasePrice: 0
        };

        this._options = options || optionDefaults;
        if (options) {
            this._options.investmentAmounts = this._options.investmentAmounts || optionDefaults.investmentAmounts;
            this._options.cashOnCashPercent = this._options.cashOnCashPercent || optionDefaults.cashOnCashPercent;
        }
    }

    public getExpectedCashOnCashAmountAnnual(amount: number): number {
        if (amount === undefined) {
            amount = this._options.investmentAmounts[0];
        }
        return fixedAndFloat(amount * (this._options.cashOnCashPercent / 100));
    }

    public saleEquityByDate(today: Date): number {
        if (!this.purchaseDate || !this.saleDate) {
            return fixedAndFloat(0);
        }

        if (this.saleDate.getTime() > today.getTime()) {
            return fixedAndFloat(0);
        }

        return this.expectedSaleEquity;
    }

    public cashOnCashAmountAnnual(today: Date): number {
        if (!isValidCashFlowTime(this.purchaseDate, this.saleDate, today)) {
            return 0;
        }

        const preMaturityDivider = 2;
        const maturityDivider = 1;
        const divider = totalMonths(this.purchaseDate, today) <= 6 ? preMaturityDivider : maturityDivider;
        return fixedAndFloat(this.getExpectedCashOnCashAmountAnnual(this.investmentAmount) / divider);
    }

    /**
     * Will return 0, partial value, or full value depending on how close the purchase date or if sold
     * @param {Date} today
     * @returns {number}
     */
    public cashOnCashAmountMonthly(today): number {
        if (!isValidCashFlowTime(this.purchaseDate, this.saleDate, today)) {
            return 0;
        }

        const num = this.cashOnCashAmountAnnual(today);
        return fixedAndFloat(num / 12);
    }

    /**
     * returns a contractual amount that the user can buy-in for being a part of the investment. This means,
     * it examines the investment amount array against the user's ledger balance. Based on the amount it has available,
     * it will return the largest amount it can invest
     * @param user
     */
    public minInvestmentAmountByUser(user: IUser): number {
        const s: number[] = this._options.investmentAmounts.sort((a, b) => a - b);
        let curMatch = 0;

        if (s.length === 0 || user.getLedgerBalance() === 0 || user.getLedgerBalance() <= s[0]) {
            return s[0];
        }

        for (const amount of s) {
            if (user.getLedgerBalance() >= amount) {
                curMatch = amount;
            }
        }

        return curMatch;
    }

    /**
     * will look at the user's willBeAbleToInvest, minEquityCapture%, and minCashOnCash%
     * @param user
     */
    public canInvestByUser(user: IUser): UserInvestResult {
        if (!user.hasMoneyToInvest()) {
            return new UserInvestResult(false, InvestmentReasons.UserHasNoMoneyToInvest);
        }

        if (user.getLedgerBalance() < this.minInvestmentAmountByUser(user)) {
            return new UserInvestResult(false, InvestmentReasons.UserHasNotSavedEnoughMoney, `user bal ${currencyFormatter(user.getLedgerBalance())} < min invest amount ${currencyFormatter(this.minInvestmentAmountByUser(user))}`);
        }

        if (this.equityCapturePercent < user.minEquityCapturePercent) {
            return new UserInvestResult(false, InvestmentReasons.DoesNotMeetUserRuleEquityCapture, `equity % ${this.equityCapturePercent} < user min equity rule ${user.minEquityCapturePercent}`);
        }

        const minInvestCashOnCashAmount = this.minInvestmentAmountByUser(user) * this.cashOnCashPercent / 100;
        if (minInvestCashOnCashAmount >= user.minMonthlyCashOnCashAmountApartment) {
            return new UserInvestResult(true, InvestmentReasons.CanInvest);
        }
        return new UserInvestResult(false, InvestmentReasons.DoesNotMeetUserRuleCashOnCash, `min invest coc ${currencyFormatter(minInvestCashOnCashAmount)} >= user min cac rule ${currencyFormatter(user.minMonthlyCashOnCashAmountApartment)}`);
    }

    public get expectedSellEquity(): number {
        const totalEquity = this._options.sellPrice - this._options.purchasePrice;
        const investmentPercent = this.investmentAmount / this._options.purchasePrice;
        return fixedAndFloat(totalEquity * investmentPercent);
    }

    public clone(): PassiveApartment {
        const rental = new PassiveApartment({...this._options});
        rental.investmentAmount = this.investmentAmount;
        rental.purchaseDate = this.purchaseDate;
        rental.id = this.id;
        rental.refinanceDate = this.refinanceDate;
        rental.saleDate = this.saleDate;
        return rental;
    }

    private readonly _options: IPassiveApartmentOptions;
}
