'use strict';
import {IRentalOptions, Rental} from './rental';
import {currencyFormatter, fixedAndFloat, isValidCashFlowTime, totalMonths} from '../utils';
import {IUser} from '../user';
import {InvestmentReasons, UserInvestResult} from './investment-reasons';

export interface ISingleFamilyOptions extends IRentalOptions {
    investmentPercent?: number;
    cashOnCashAmountMonthly?: number;
}

export class SingleFamily extends Rental {

    get cashOnCashPercent(): number {
        return this._options.cashOnCashAmountMonthly * 12 / this.purchasePrice * 100;
    }

    constructor(options: ISingleFamilyOptions = {}) {
        super(options);
        this.id = `SF${this.id}`;
        const optionDefaults: ISingleFamilyOptions = {
            investmentPercent: 50,
            cashOnCashAmountMonthly: 0,
            purchaseDate: null,
            minSaleYears: 0,
            saleDate: null,
            sellPrice: 0,
            refinancePercent: 0,
            purchasePrice: 0,
            minRefinanceMonths: 0
        };

        this._options = options || optionDefaults;
        if (options) {
            this._options.investmentPercent = options.investmentPercent || optionDefaults.investmentPercent;
            this._options.cashOnCashAmountMonthly = this._options.cashOnCashAmountMonthly || optionDefaults.cashOnCashAmountMonthly;
        }
    }

    public cashOnCashAmountAnnual(today: Date): number {
        if (!isValidCashFlowTime(this.purchaseDate, this.saleDate, today)) {
            return 0;
        }

        return fixedAndFloat(this.cashOnCashAmountMonthly(today) * 12);
    }

    public cashOnCashAmountMonthly(today: Date): number {
        if (!isValidCashFlowTime(this.purchaseDate, this.saleDate, today)) {
            return 0;
        }

        const preMaturityDivider = 2;
        const maturityDivider = 1;
        const divider = totalMonths(this.purchaseDate, today) <= 6 ? preMaturityDivider : maturityDivider;

        return fixedAndFloat(this._options.cashOnCashAmountMonthly / divider);
    }

    /**
     * evaluates the amount by taking the getting the amount by the investment percent from purchase price
     */
    public minInvestmentAmountByUser(user: IUser): number {
        return this._options.investmentPercent / 100 * this.purchasePrice;
    }

    public canInvestByUser(user: IUser): UserInvestResult {
        if (!user.hasMoneyToInvest()) {
            return new UserInvestResult(false, InvestmentReasons.UserHasNoMoneyToInvest);
        }

        if (user.getAllowedInvestmentLedgerBalance() < this.minInvestmentAmountByUser(user)) {
            return new UserInvestResult(false, InvestmentReasons.UserHasNotSavedEnoughMoney, `user allowed bal ${currencyFormatter(user.getAllowedInvestmentLedgerBalance())} < min invest amount ${currencyFormatter(this.minInvestmentAmountByUser(user))}`);
        }

        if (this.equityCapturePercent < user.minEquityCapturePercent) {
            return new UserInvestResult(false, InvestmentReasons.DoesNotMeetUserRuleEquityCapture, `equity % ${this.equityCapturePercent} < user min equity rule ${user.minEquityCapturePercent}`);
        }

        if (this._options.cashOnCashAmountMonthly >= user.minMonthlyCashOnCashAmountHouse) {
            return new UserInvestResult(true, InvestmentReasons.CanInvest)
        }

        return new UserInvestResult(false, InvestmentReasons.DoesNotMeetUserRuleCashOnCash, `min invest coc ${currencyFormatter(this._options.cashOnCashAmountMonthly)} >= user min cac rule ${currencyFormatter(user.minMonthlyCashOnCashAmountHouse)}`);
    }

    public getExpectedCashOnCashAmountAnnual(amount: number): number {
        return this._options.cashOnCashAmountMonthly * 12;
    }

    public get expectedSellEquity(): number {
        const totalEquity = this._options.sellPrice - this._options.purchasePrice - this.investmentAmount;
        return fixedAndFloat(totalEquity);
    }

    public clone(): SingleFamily {
        const rental = new SingleFamily({...this._options});
        rental.purchaseDate = this.purchaseDate;
        rental.investmentAmount = this.investmentAmount;
        rental.id = this.id;
        rental.refinanceDate = this.refinanceDate;
        rental.saleDate = this.saleDate;
        return rental;
    }

    public get reserveRequirementMonthly(): number {
        if (!this.saleDate) {
            return 0;
        }

        const yearOnLoan: number = 30;
        const monthsOnLoan: number = yearOnLoan * 12;

        const loanRatePercent: number = 4.1;

        // TODO: figure out a way to reduce this complexity
        const amountOnLoan: number = this.purchasePrice / 25 * 3;
        let rate: number = loanRatePercent / 100 / 12;
        rate = rate * amountOnLoan / (1 - ((1 + rate) * (monthsOnLoan * -1)));

        const avgTexasPropertyTaxPercent: number = 1.81;
        const taxes: number = this.purchasePrice * avgTexasPropertyTaxPercent / 100;

        const fakeRateCalculation: number = 0.0053097345132743;
        const annualInsurance: number = fixedAndFloat(this.purchasePrice * fakeRateCalculation);

        rate = rate + (taxes / 12) + (annualInsurance / 12);

        return fixedAndFloat(rate);
    }

    private readonly _options: ISingleFamilyOptions;
}
