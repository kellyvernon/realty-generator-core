import {IUser} from '../user';

export interface IRentalCashFlow {
    id: string;
    readonly cashOnCashPercent: number;
    readonly expectedCashOnCash: number;
    investmentAmount: number;

    cashOnCashAmountAnnual(today: Date): number;

    cashOnCashAmountMonthly(today: Date): number;

    /**
     * evaluates the amount by taking the getting the amount by the investment percent from purchase price
     */
    // eslint-disable-next-line no-unused-vars
    minInvestmentAmountByUser(user: IUser): number;

    getExpectedCashOnCashAmountAnnual(amount: number): number;
}
