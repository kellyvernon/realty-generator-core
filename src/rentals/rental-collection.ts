'use strict';

import * as momentRange from 'moment-range';
import * as moment from 'moment';

import {fixedAndFloat, isActiveDate} from '../utils';
import {IRental} from './rental';
import {RentalTypes} from './rental-types';
import {IRentalCashFlow} from './i-rental-cash-flow';
import {Collection} from '../common/collection';

const extendMoment = momentRange.extendMoment(moment);

export class RentalCollection extends Collection<IRental> {
    /**
     * returns an array of collection that have been sold
     */
    public getSoldRentals(): IRental[] {
        if (!this.isPopulated()) {
            return [];
        }
        return this.filter(ele => ele.saleDate !== null);
    }

    public cashOnCashAmountMonthly(today: Date): number {
        if (!this.isPopulated()) {
            return 0;
        }

        return this.currentCashFlowByRental(today).reduce((total, entity) => {
            return total + entity.cashOnCashAmountMonthly(today);
        }, 0);
    }

    public totalMonths(): number {
        if (!this.isPopulated()) {
            return -1;
        }

        const start = this[0].purchaseDate;
        const end = this.last().purchaseDate;

        const range = extendMoment.range(start, end);

        return Array.from(range.by('month')).length - 1;
    }

    public rentalsForSale(today: Date): IRental[] {
        return this.filter(element => element.canSell(today));
    }


    /**
     * will always return an array (empty or populated)
     * @param today
     */
    public currentCashFlowByRental(today: Date): IRentalCashFlow[] {
        if (!this.isPopulated()) {
            return [];
        }

        return this.filter(element => element.cashOnCashAmountMonthly(today) > 0);
    }

    public activeRentals(today: Date): IRental[] {
        if (!this.isPopulated()) {
            return [];
        }

        return this.filter(rental => isActiveDate(today, rental.purchaseDate, rental.saleDate));
    }

    public rentalCollectionForSale(today: Date): number[] {
        return this.map((entity, index) => ({index, rental: entity})).filter(entity => {
            if (entity.rental.canSell(today)) {
                return entity;
            }

            return null;
        }).filter(value => value !== null).map(entity => entity.index);
    }

    public rentalsForRefinance(today: Date): number[] {
        return this.map((entity, index) => ({index, rental: entity})).filter(entity => {
            if (entity.rental.canRefinance(today)) {
                return entity;
            }

            return null;
        }).filter(value => value !== null).map(entity => entity.index);
    }

    public get(index: number): IRental {
        return this[index];
    }

    public totalYears(): number {
        if (!this.isPopulated()) {
            return -1;
        }

        return this.totalMonths() / 12;
    }

    public reserveRequirementMonthly():number{
        if (!this.isPopulated()) {
            return 0;
        }

        const singleFamilyReserves = this.filter(x => x.getRentalType() === RentalTypes.SingleFamily).reduce((total, entity) => {
            return total + entity.reserveRequirementMonthly * 6;
        }, 0);

        const others = this.filter(x => x.getRentalType() !== RentalTypes.SingleFamily).reduce((total, entity) => {
            return total + entity.reserveRequirementMonthly;
        }, 0);

        return fixedAndFloat(singleFamilyReserves + others);
    }

    public clone(): RentalCollection {
        const rentalCollection = new RentalCollection();
        this.forEach(x => {
            rentalCollection.push(x.clone());
        });
        return rentalCollection;
    }
}

