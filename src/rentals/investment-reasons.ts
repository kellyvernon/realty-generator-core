export enum InvestmentReasons {
    Unknown,
    CanInvest,
    UserHasNoMoneyToInvest,
    UserHasNotSavedEnoughMoney,
    DoesNotMeetUserRuleEquityCapture,
    DoesNotMeetUserRuleCashOnCash
}

export class UserInvestResult {

    get message(): string {
        const theMessage = this._message ? ` ${this._message}` : '';
        return `${InvestmentReasons[this.investmentReason]}${theMessage}`;
    }

    public investmentReason: InvestmentReasons;
    public canInvest: boolean;

    constructor(canInvest: boolean, reason: InvestmentReasons = InvestmentReasons.Unknown, message: string = '') {
        this.investmentReason = reason;
        this.canInvest = canInvest;
        this._message = message;
    }

    public toString(): string {
        return this.message;
    }

    private readonly _message: string;
}
