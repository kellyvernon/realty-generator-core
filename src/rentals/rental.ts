'use strict';
import {v4} from 'uuid';
import {IUser} from '../user';

import {fixedAndFloat, isValidCashFlowTime} from '../utils';
import {RentalTypes} from './rental-types';
import {IRentalCashFlow} from './i-rental-cash-flow';
import {IRentalSale} from './i-rental-sale';
import {UserInvestResult} from './investment-reasons';

export interface IRentalOptions {
    purchaseDate?: Date;
    purchasePrice?: number;

    minSaleYears?: number;
    saleDate?: Date;
    sellPrice?: number;

    refinancePercent?: number;
    minRefinanceMonths?: number;
}

export interface IRental extends IRentalCashFlow {
    id: string;
    refinanceDate?: Date;
    purchaseDate?: Date;
    saleDate?: Date;

    readonly purchasePrice: number;
    readonly minSellDate: Date;
    readonly minSaleYears: number;
    readonly minRefinanceMonths: number;
    readonly expectedSellEquity: number;
    readonly sellPrice: number;
    readonly equityCapturePercent: number;
    readonly investmentPercent: number;
    readonly minRefinanceDate: Date;
    readonly expectedRefinanceEquity: number;
    readonly reserveRequirementMonthly: number;

    canSell(today: Date): boolean;

    getSellEquityByDate(today: Date): any;

    canInvestByUser(user: IUser): UserInvestResult;

    getInvestmentPercent(amount: number): number;

    getRefinanceAmountByDate(today: Date): number;

    canRefinance(today: Date): boolean;

    saleEquityByDate(today: Date): number;

    getRentalType(): RentalTypes;

    clone(): IRental;
}

export class Rental implements IRental, IRentalCashFlow, IRentalSale {

    get id(): string {
        return this._id;
    }

    set id(value: string) {
        this._id = value;
    }

    get cashOnCashPercent(): number {
        return 0;
    }

    get purchasePrice(): number {
        return this._rentalOptions.purchasePrice;
    }

    get expectedCashOnCash(): number {
        return 0;
    }

    get purchaseDate(): Date {
        return this._rentalOptions.purchaseDate;
    }

    set purchaseDate(value: Date) {
        this._rentalOptions.purchaseDate = value;
    }

    get minSellDate(): Date {
        return new Date(
            this.purchaseDate.getUTCFullYear() + this._rentalOptions.minSaleYears,
            this.purchaseDate.getUTCMonth(),
            1);
    }

    get minSaleYears(): number {
        return this._rentalOptions.minSaleYears;
    }

    get minRefinanceMonths(): number {
        return this._rentalOptions.minRefinanceMonths;
    }

    /**
     * TODO: this doesn't work for apartments (sf prolly...), I should have something that simply gives back the final amount..
     * TODO: ... who cares what the sale price is, just go with what you expect back!
     */
    get expectedSellEquity(): number {
        const totalEquity = this._rentalOptions.sellPrice - this._rentalOptions.purchasePrice;
        const investmentPercent = this.investmentAmount / this._rentalOptions.purchasePrice;
        return fixedAndFloat(totalEquity * investmentPercent);
    }

    get saleDate(): Date {
        return this._saleDate;
    }

    set saleDate(value: Date) {
        this._saleDate = value;
    }

    get investmentAmount() {
        return this._investmentAmount;
    }

    set investmentAmount(value) {
        this._investmentAmount = value;
    }

    get sellPrice(): number {
        return this._rentalOptions.sellPrice;
    }

    /**
     * evaluates the purchase price against the sellPrice
     */
    get equityCapturePercent(): number {
        if (this.sellPrice <= 0) {
            return 0;
        }

        return fixedAndFloat(this._rentalOptions.sellPrice / this._rentalOptions.purchasePrice * 100);
    }

    get investmentPercent(): number {
        return this.getInvestmentPercent(this.investmentAmount);
    }

    get minRefinanceDate(): Date {
        return new Date(
            this.purchaseDate.getUTCFullYear(),
            this.purchaseDate.getUTCMonth() + this._rentalOptions.minRefinanceMonths,
            1);
    }

    get expectedRefinanceEquity(): number {
        return fixedAndFloat(this.investmentAmount * (this._rentalOptions.refinancePercent / 100));
    }

    get reserveRequirementMonthly(): number {
        return 0;
    }

    public refinanceDate?: Date;

    constructor(options: IRentalOptions = {}) {
        if (this.constructor.name === 'Rental') {
            throw new Error('abstract');
        }
        this._id = v4().split('-')[0];

        this._rentalOptions = {
            purchasePrice: options && !!options.purchasePrice ? options.purchasePrice : 0,
            sellPrice: options && !!options.sellPrice ? options.sellPrice : 0,
            purchaseDate: options && !!options.purchaseDate ? options.purchaseDate : null,
            minSaleYears: options && !!options.minSaleYears ? options.minSaleYears : 0,
            refinancePercent: options && !!options.refinancePercent ? options.refinancePercent : 0,
            minRefinanceMonths: options && !!options.minRefinanceMonths ? options.minRefinanceMonths : 0
        };

        this.refinanceDate = null;

        this._investmentAmount = 0;
    }

    public canSell(today: Date): boolean {
        if (this.saleDate) {
            return false;
        }

        return today.getTime() >= this.minSellDate.getTime();
    }

    // eslint-disable-next-line no-unused-vars
    public cashOnCashAmountAnnual(today: Date): number {
        return 0;
    }

    // eslint-disable-next-line no-unused-vars
    public cashOnCashAmountMonthly(today: Date): number {
        return 0;
    }

    /**
     * evaluates the amount by taking the getting the amount by the investment percent from purchase price
     */
    // eslint-disable-next-line no-unused-vars
    public minInvestmentAmountByUser(user: IUser): number {
        return 0;
    }

    public getSellEquityByDate(today: Date) {
        if (this.saleDate) {
            return 0;
        }

        if (today.getTime() >= this.minSellDate.getTime()) {
            return this.expectedSellEquity;
        }

        return 0;
    }

    // eslint-disable-next-line no-unused-vars
    public canInvestByUser(user: IUser): UserInvestResult {
        return new UserInvestResult(false);
    }

    public getInvestmentPercent(amount: number): number {
        const totalEquity = this._rentalOptions.sellPrice - this._rentalOptions.purchasePrice;
        const investmentPercent = amount / this._rentalOptions.purchasePrice;
        return fixedAndFloat(totalEquity * investmentPercent);
    }

    public getRefinanceAmountByDate(today: Date): number {
        if (this.refinanceDate) {
            return 0;
        }

        if (today.getTime() > this.minRefinanceDate.getTime()) {
            return this.expectedRefinanceEquity;
        }

        return 0;
    }

    public canRefinance(today: Date): boolean {
        if (!isValidCashFlowTime(this.purchaseDate, this.saleDate, today)) {
            return false;
        }

        if (this.refinanceDate) {
            return false;
        }

        return today.getTime() > this.minRefinanceDate.getTime();
    }

    public getExpectedCashOnCashAmountAnnual(amount: number): number {
        return 0;
    }

    public saleEquityByDate(today: Date): number {
        if (!this.purchaseDate || !this.saleDate) {
            return fixedAndFloat(0);
        }

        if (this.saleDate.getTime() > today.getTime()) {
            return fixedAndFloat(0);
        }

        return this.sellPrice - this.purchasePrice;
    }

    public getRentalType(): RentalTypes {
        return RentalTypes[this.constructor.name];
    }

    public clone(): IRental {
        const rental = new Rental({...this._rentalOptions});
        rental.purchaseDate = this.purchaseDate;
        rental.investmentAmount = this.investmentAmount;
        rental.id = this.id;
        rental.refinanceDate = this.refinanceDate;
        rental.saleDate = this.saleDate;
        return rental;
    }

    private _saleDate: Date;
    private _id: string;
    private _investmentAmount: number;
    private readonly _rentalOptions: IRentalOptions;
}
