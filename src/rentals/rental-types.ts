export enum RentalTypes {
    Unknown = 0,
    Rental,
    SingleFamily,
    PassiveApartment
}