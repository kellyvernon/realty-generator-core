export interface IRentalSale {
    id: string;
    readonly cashOnCashPercent: number;
    readonly minSellDate: Date;
    readonly minSaleYears: number;
    readonly expectedSellEquity: number;
    saleDate: Date;
    readonly sellPrice: number;
    readonly equityCapturePercent: number;

    canSell(today: Date): boolean;

    getSellEquityByDate(today: Date): any;

    saleEquityByDate(today: Date): number;
}
