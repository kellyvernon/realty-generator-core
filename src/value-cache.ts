'use strict';

const getNextExpire = (current, cacheExpireDate, advanceInMonths) => {
    if (!current) {
        return cacheExpireDate;
    }

    return new Date(Date.UTC(
        current.getUTCFullYear(),
        current.getUTCMonth() + advanceInMonths,
        current.getUTCDate()));
};

export interface IValueCache {
    renewalInMonths: number;
    expireDate: Date;
    readonly newDefault: any;

    setValue(value: any, currentDate?: Date): void;

    /**
     * if the cache is expired, then it will return the default value. otherwise
     * it will return the stored value
     */
    getValue(currentTime?: Date): any;

    /**
     * determines if the expiredDate is greater than
     * or equal to currentDate. if this is the case it
     * will be return true. Also if expiredDate is nothing,
     * then it will return true. Also if currentDate is null,
     * while expiredDate exists then it will be expired, aka true
     * @param {Date|null|undefined} currentDate
     * @returns {boolean}
     */
    isCacheExpired(currentDate?: Date): boolean;
}

export class ValueCache implements IValueCache {

    get renewalInMonths(): number {
        return this._renewalInMonths;
    }

    set renewalInMonths(value) {
        this._renewalInMonths = value;
    }

    get expireDate(): Date {
        return this._expireDate;
    }

    set expireDate(value) {
        if (value) {
            value = new Date(Date.UTC(
                value.getUTCFullYear(),
                value.getUTCMonth(),
                value.getUTCDate()));
        }
        this._expireDate = value;
    }

    get newDefault(): any {
        if (Array.isArray(this._defaultValue)) {
            return [...this._defaultValue];
        }

        return JSON.parse(JSON.stringify(this._defaultValue));
    }

    constructor(expireDate: Date, defaultValue: any, renewalInMonths ?: number) {
        this._expireDate = expireDate;
        this._defaultValue = defaultValue;
        this._renewalInMonths = renewalInMonths;
        this._value = this.newDefault;
    }

    public setValue(value: any, currentDate?: Date): void {
        if (this.isCacheExpired(currentDate)) {
            this.expireDate = getNextExpire(currentDate, this.expireDate, this._renewalInMonths);
            this._value = this.newDefault;
        }

        this._value = value;
    }

    /**
     * if the cache is expired, then it will return the default value. otherwise
     * it will return the stored value
     */
    public getValue(currentTime?: Date): any {
        if (!this.isCacheExpired(currentTime)) {
            return this._value;
        }

        this.setValue(this.newDefault, currentTime);
        return this._value;
    }

    /**
     * determines if the expiredDate is greater than
     * or equal to currentDate. if this is the case it
     * will be return true. Also if expiredDate is nothing,
     * then it will return true. Also if currentDate is null,
     * while expiredDate exists then it will be expired, aka true
     * @param {Date|null|undefined} currentDate
     * @returns {boolean}
     */
    public isCacheExpired(currentDate?: Date): boolean {
        if (!this.expireDate) {
            return false;
        }

        if (this.expireDate && !currentDate) {
            return true;
        }

        return this.expireDate.getTime() < currentDate.getTime();
    }

    protected _value: any;
    private _expireDate: Date;
    private readonly _defaultValue: any;
    private _renewalInMonths: number;
}
