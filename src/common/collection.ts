export class Collection<T> extends Array<T> {
    public first(): T | null {
        if (this.isPopulated()) {
            return this[0];
        }

        return null;
    }

    public last(): T | null {
        if (this.isPopulated()) {
            return this[this.length - 1];
        }

        return null;
    }

    public isPopulated(): boolean {
        return this.length > 0;
    }
}
